// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require jquery
//= require jquery_ujs
//= require jquery.turbolinks
//= require bootstrap-sprockets
//= require turbolinks
//= require jquery.countdown
//= require jquery.countdown-zh-CN
//= require jquery.countdown-zh-TW
//= require jquery.countdown-ja
//= require jquery.countdown-ko
//= require jquery.countdown-vi
//= require jquery.countdown-fr
//= require select2-full
//= require select2_locale_en
//= require select2_locale_zh-CN
//= require select2_locale_zh-TW
//= require select2_locale_ja
//= require trix
//= require moment
//= require moment/zh-cn
//= require moment/ja
//= require moment/fr
//= require moment/ko
//= require moment/vi
//= require moment/zh-tw
//= require bootstrap-datetimepicker
//= require bootstrap-table
//= require bootstrap-table-locale-all
//= require tableExport.jquery.plugin
//= require extensions/bootstrap-table-export
//= require extensions/bootstrap-table-mobile
//= require extensions/bootstrap-table-filter-control
//= require cocoon
//= require_tree .
