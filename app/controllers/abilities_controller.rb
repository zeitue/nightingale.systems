class AbilitiesController < ApplicationController
  before_action :set_ability, only: [:show, :edit, :update, :destroy]
  #before_action :admin?, only: [:new, :edit, :create, :update, :destroy]
  before_action :usable?

  # GET /abilities
  # GET /abilities.json
  def index
    if current_user.superuser?
      @abilities = Ability.all.page(params[:page]).per(8)
    else
      @abilities = Ability.domained(current_user).page(params[:page]).per(8)
    end
  end

  # GET /abilities/1
  # GET /abilities/1.json
  def show
  end

  # GET /abilities/new
  def new
    @ability = Ability.new
  end

  # GET /abilities/1/edit
  def edit
  end

  # POST /abilities
  # POST /abilities.json
  def create
    @ability = Ability.new(ability_params)
    Mobility.with_locale(params[:lang]) do
      respond_to do |format|
        if @ability.save
          format.html { redirect_to @ability, notice: t('.notice') }
          format.json { render :show, status: :created, location: @ability }
        else
          format.html { render :new, lang: params[:lang] }
          format.json { render json: @ability.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # PATCH/PUT /abilities/1
  # PATCH/PUT /abilities/1.json
  def update
    Mobility.with_locale(params[:lang]) do
      respond_to do |format|
        if @ability.update(ability_params)
          format.html { redirect_to @ability, notice: t('.notice') }
          format.json { render :show, status: :ok, location: @ability }
        else
          format.html { render :edit, lang: params[:lang] }
          format.json { render json: @ability.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # DELETE /abilities/1
  # DELETE /abilities/1.json
  def destroy
    @ability.destroy
    respond_to do |format|
      format.html { redirect_to abilities_url, notice: t('.notice') }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ability
      @ability = Ability.find(params[:id])
    end

    def admin?
      if !current_user.admin?
        redirect_to abilities_path, :flash => { :danger => t('access_denied') }
      end
    end

    def usable?
      unless current_user.use_mastery_builder?
        redirect_to root_path, :flash => { :danger => t('access_denied') }
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def ability_params
      params.require(:ability).permit(:title, :category, :required_level, :proficiency_level, :domain_id, abilityset_ids: [], abilitysets_attributes: [:id, :title, :level, :domain_id])
    end
end
