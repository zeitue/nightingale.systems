class AbilitysetsController < ApplicationController
  before_action :set_abilityset, only: [:show, :edit, :update, :destroy]
  #before_action :admin?, only: [:new, :edit, :create, :update, :destroy]
  before_action :usable?

  # GET /abilitysets
  # GET /abilitysets.json
  def index
    if current_user.superuser?
      @abilitysets = Abilityset.all.page(params[:page]).per(8)
    else
      @abilitysets = Abilityset.domained(current_user).page(params[:page]).per(8)
    end
  end

  # GET /abilitysets/1
  # GET /abilitysets/1.json
  def show
  end

  # GET /abilitysets/new
  def new
    @abilityset = Abilityset.new
  end

  # GET /abilitysets/1/edit
  def edit
  end

  # POST /abilitysets
  # POST /abilitysets.json
  def create
    @abilityset = Abilityset.new(abilityset_params)
    Mobility.with_locale(params[:lang]) do
      respond_to do |format|
        if @abilityset.save
          format.html { redirect_to @abilityset, notice: t('.notice') }
          format.json { render :show, status: :created, location: @abilityset }
        else
          format.html { render :new, lang: params[:lang] }
          format.json { render json: @abilityset.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # PATCH/PUT /abilitysets/1
  # PATCH/PUT /abilitysets/1.json
  def update
    Mobility.with_locale(params[:lang]) do
      respond_to do |format|
        if @abilityset.update(abilityset_params)
          format.html { redirect_to @abilityset, notice: t('.notice') }
          format.json { render :show, status: :ok, location: @abilityset }
        else
          format.html { render :edit, lang: params[:lang] }
          format.json { render json: @abilityset.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # DELETE /abilitysets/1
  # DELETE /abilitysets/1.json
  def destroy
    @abilityset.destroy
    respond_to do |format|
      format.html { redirect_to abilitysets_url, notice: t('.notice') }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_abilityset
      @abilityset = Abilityset.find(params[:id])
    end

    def admin?
      if !current_user.admin?
        redirect_to abilitysets_path, :flash => { :danger => t('access_denied') }
      end
    end

    def usable?
      unless current_user.use_mastery_builder?
        redirect_to root_path, :flash => { :danger => t('access_denied') }
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def abilityset_params
      params.require(:abilityset).permit(:title, :level, :domain_id, ability_ids: [], abilities_attributes: [:id, :title, :category, :required_level, :proficiency_level, :domain_id], mastery_ids: [], masteries_attributes: [:id, :title, :domain_id])
    end
end
