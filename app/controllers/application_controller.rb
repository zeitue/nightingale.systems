class ApplicationController < ActionController::Base
  before_action :configure_permitted_parameters, if: :devise_controller?
  protect_from_forgery with: :exception
  before_action :authenticate_user!
  before_action :set_locale
  before_action :set_timezone
  helper_method :mailbox, :conversation
  helper_method :available_locales
  rescue_from ActiveRecord::RecordNotFound, :with => :record_not_found
  rescue_from ActiveRecord::InvalidForeignKey, :with => :fk_contraint

  def set_locale
    I18n.locale = extract_locale_from_user || params[:locale] || extract_locale_from_subdomain || extract_locale_from_tld || I18n.default_locale
  end

  def extract_locale_from_user
    current_user.language if current_user
  end

  def extract_locale_from_tld
    parsed_locale = request.host.split('.').last
    I18n.available_locales.map(&:to_s).include?(parsed_locale) ? parsed_locale : nil
  end

  def extract_locale_from_subdomain
    parsed_locale = request.subdomains.first
    I18n.available_locales.map(&:to_s).include?(parsed_locale) ? parsed_locale : nil
  end

  def default_url_options
    { locale: I18n.locale }
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:prefix, :first_name, :middle_name, :last_name, :suffix, :language, :timezone, :pin, :level, :unit, :extid, :account_type, :birthdate, :domain_id, {avatars: []}, :avatars_cache
])
    devise_parameter_sanitizer.permit(:account_update, keys: [:prefix, :first_name, :middle_name, :last_name, :suffix, :language, :timezone, :pin, :level, :unit, :extid, :account_type, :birthdate, :domain_id, {avatars: []}, :avatars_cache
])
  end

  def set_timezone
    if current_user && !current_user.timezone.empty?
      Time.zone = current_user.timezone
    end
  end

  def available_locales
    hidden = [:zh]
    (I18n.available_locales - hidden)
  end

  private

  def mailbox
    @mailbox ||= current_user.mailbox
  end

  def conversation
    @conversation ||= mailbox.conversations.find(params[:id])
  end

  def record_not_found
    flash[:warning] = t('record_not_found')
    redirect_to action: :index
  end

  def fk_contraint
    flash[:warning] = t('fk_contraint')
    redirect_to action: :index
  end
end
