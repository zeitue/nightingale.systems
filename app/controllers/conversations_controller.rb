class ConversationsController < ApplicationController
  before_action :usable?
  def new
  end

  def index
    redirect_to mailbox_inbox_path
  end
  def create
    if !conversation_params[:recipients].empty? && !conversation_params[:body].empty? && !conversation_params[:subject].empty?
      recipients = User.where(id: conversation_params[:recipients])
      conversation = current_user.send_message(recipients, conversation_params[:body], conversation_params[:subject]).conversation
      flash[:notice] = t('.notice')
      redirect_to conversation_path(conversation)
    else
      flash[:alert] = t('.error')
      render :action => :new
    end
  end

  def show
    @receipts = conversation.receipts_for(current_user).order("created_at DESC")
    # mark conversation as read
    conversation.mark_as_read(current_user)
  end


  def reply
    current_user.reply_to_conversation(conversation, message_params[:body])
    flash[:notice] = t('.notice')
    redirect_to conversation_path(conversation)
  end

  def trash
    conversation.move_to_trash(current_user)
    redirect_to mailbox_inbox_path
  end

  def untrash
    conversation.untrash(current_user)
    redirect_to mailbox_inbox_path
  end

  def empty_trash
    current_user.mailbox.trash.each do |conversation|
      conversation.receipts_for(current_user).update_all(:deleted => true)
    end
    redirect_to mailbox_inbox_path
  end

  private

  def conversation_params
    params.require(:conversation).permit(:subject, :body,recipients:[])
  end

  def usable?
    unless current_user.use_mail?
      redirect_to root_path, :flash => { :danger => t('access_denied') }
    end
  end
  def message_params
    params.require(:message).permit(:body, :subject)
  end
end
