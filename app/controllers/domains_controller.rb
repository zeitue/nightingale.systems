class DomainsController < ApplicationController
  before_action :set_domain, only: [:show, :edit, :update, :destroy]
  before_action :super_only, only: [:new, :destroy]
  before_action :domain_admin, only: [:edit, :update]

  # GET /domains
  # GET /domains.json
  def index
    if current_user.superuser?
      @domains = Domain.all.page(params[:page]).per(8)
    else
      redirect_to current_user.domain
    end
  end

  # GET /domains/1
  # GET /domains/1.json
  def show
  end

  # GET /domains/new
  def new
    @domain = Domain.new
  end

  # GET /domains/1/edit
  def edit
  end

  # POST /domains
  # POST /domains.json
  def create
    @domain = Domain.new(domain_params)
    Mobility.with_locale(params[:lang]) do
      respond_to do |format|
        if @domain.save
          format.html { redirect_to @domain, notice: t('.notice') }
          format.json { render :show, status: :created, location: @domain }
        else
          format.html { render :new, lang: params[:lang] }
          format.json { render json: @domain.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # PATCH/PUT /domains/1
  # PATCH/PUT /domains/1.json
  def update
    #available_locales.each do |lo|
     # Mobility.with_locale(lo) do
     #   @domain.send('name=', params['domain']['name'][lo])
     # end
    #end
    Mobility.with_locale(params[:lang]) do
      respond_to do |format|
        if @domain.update(domain_params)
          format.html { redirect_to @domain, notice: t('.notice') }
          format.json { render :show, status: :ok, location: @domain }
        else
          format.html { render :edit, lang: params[:lang] }
          format.json { render json: @domain.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # DELETE /domains/1
  # DELETE /domains/1.json
  def destroy
    @domain.destroy
    respond_to do |format|
      format.html { redirect_to domains_url, notice: t('.notice') }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_domain
      @domain = Domain.find(params[:id])
    end

    def super_only
      if !current_user.superuser?
        redirect_to domains_path, :flash => { :danger => t('access_denied') }
      end
    end

    def domain_admin
      if !current_user.superuser? && !(current_user.domain_id == @domain.id && current_user.admin?)
        redirect_to domains_path, :flash => { :danger => t('access_denied') }
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def domain_params
      params.require(:domain).permit(:name, :namespace, :language, :timezone,
                                     :country, :student_starting_level,
                                     :map_provider, :student_form_fill,
                                     :overseer_form_fill,
                                     :overseer_agreement_form_fill,
                                     :student_form_id, :overseer_form_id,
                                     :overseer_agreement_form_id, :time_margin,
                                     :demo)
    end
end
