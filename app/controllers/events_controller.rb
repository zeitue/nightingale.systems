class EventsController < ApplicationController
  before_action :set_event, only: [:show, :edit, :update, :destroy, :login, :logout, :login_update, :logout_update, :participants]
  before_action :set_events, only: [:calendar]
  before_action :has_power?, only: [:new, :create, :edit, :update, :destroy]
  before_action :usable?
  after_action :check_has?, only: [:create, :update]

  # GET /events
  # GET /events.json
  def index
    if session[:event_view]
      redirect_to session[:event_view]
    else
      session[:event_view] = calendar_events_path
      redirect_to session[:event_view]
    end
  end

  def list
    session[:event_view] = list_events_path
    if params[:date]
      point = Time.zone.parse(params[:date])
    else
      point = Time.zone.now
    end
    b = point.beginning_of_day.utc
    e = point.end_of_day.utc

    if current_user.superuser?
      @events = Event.where('start between ? and ?', b, e).page(params[:page]).per(8)
    else
      @events = Event.domained(current_user).joins(:participants).where('participants.user_id = ? or events.user_id = ?', current_user.id, current_user.id).where('(start between ? and ?)', b, e).distinct.page(params[:page]).per(8)
    end
  end

  def tasks
    session[:event_view] = tasks_events_path
    @events = Participant.includes(:event).where('(participants.user_id = ? OR events.user_id = ?)', current_user.id, current_user.id).order('events.start').references(:event).distinct.page(params[:page]).per(8)
  end

  def calendar
    session[:event_view] = calendar_events_path
  end

  def log
    session[:event_view] = log_events_path
    @events = Participant.includes(:event).where('(participants.completed = false) AND (participants.user_id = ? OR events.user_id = ?)', current_user.id, current_user.id).order('events.start').references(:event).distinct.page(params[:page]).per(8)

  end

  def login
    involved?(@event)
  end

  def logout
    involved?(@event)
  end

  def login_updates
    h = Participant.where(id: params[:participant][:id]).first
    if h
      involved?(h.event)
      h.update_attributes(login: DateTime.now())
      redirect_to tasks_events_path, notice: t('.notice')
    end
  end

  def logout_updates
    h = Participant.where(id: params[:participant][:id]).first
    if h
      involved?(h.event)
      h.update_attributes(logout: DateTime.now())
      h.check_complete?
      redirect_to tasks_events_path, notice: t('.notice')
    end
  end

  def login_update
    involved?(@event)
    h = Participant.where(id: params[:participant][:id]).first
    if h
      h.update_attributes(login: DateTime.now())
      h.check_complete?
      redirect_to participants_events_path(@event), notice: t('.notice')
    end
  end

  def logout_update
    involved?(@event)
    h = Participant.where(id: params[:participant][:id]).first
    if h
      h.update_attributes(logout: DateTime.now())
      h.check_complete?
      redirect_to participants_events_path(@event), notice: t('.notice')
    end
  end

  def participants
  end

  # GET /events/1
  # GET /events/1.json
  def show
    involved?(@event)
  end

  # GET /events/new
  def new
    @event = Event.new
  end

  # GET /events/1/edit
  def edit
  end

  # POST /events
  # POST /events.json
  def create
    @event = Event.new(event_params)

    respond_to do |format|
      if @event.save
        format.html { redirect_to @event, notice: t('.notice') }
        format.json { render :show, status: :created, location: @event }
      else
        format.html { render :new }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /events/1
  # PATCH/PUT /events/1.json
  def update
    respond_to do |format|
      if @event.update(event_params)
        format.html { redirect_to @event, notice: t('.notice') }
        format.json { render :show, status: :ok, location: @event }
      else
        format.html { render :edit }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /events/1
  # DELETE /events/1.json
  def destroy
    @event.destroy
    respond_to do |format|
      format.html { redirect_to events_url, notice: t('.notice') }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_event
    @event = Event.find(params[:id])
  end

  def set_events
    if current_user.superuser?
      @events = Event.all
    else
      @events = Event.domained(current_user).joins(:participants).where('participants.user_id = ? or events.user_id = ?', current_user.id, current_user.id).distinct
    end
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def event_params
    params.require(:event).permit(:overseer_id, :student_id, :location_id, :start, :stop, :login, :logout, :user_id, :abilityset_id, :domain_id, :student_evaulation_id, :overseer_evaluation_id, :overseer_agreement_id, participants_attributes: Participant.attribute_names.map(&:to_sym).push(:tex).push(:_destroy))
  end

  def involved?(e)
    unless (e.part_of?(current_user) || current_user.admin?)
      redirect_to events_path, :flash => { :danger => t('access_denied') }
    end
  end

  def check_has?
    if @event.abilityset
      @event.users.each do |u|
        if u.has_mastery?
          @event.abilityset.abilities.each do |ability|
            ability.knows.where(user_id: u.id).first_or_create
          end
        end
      end
    end
  end

  def has_power?
    unless current_user.events_create_edit?
      redirect_to events_path, :flash => { :danger => t('access_denied') }
    end
  end

  def usable?
    unless current_user.use_events?
      redirect_to root_path, :flash => { :danger => t('access_denied') }
    end
  end
end
