class FormsController < ApplicationController
  before_action :set_form, only: [:show, :edit, :update, :destroy]
  before_action :has_power?, only: [:edit, :update, :destroy, :new]
  before_action :usable?

  # GET /forms
  # GET /forms.json
  def index
    @forms = Form.all.page(params[:page]).per(8)
  end

  # GET /forms/1
  # GET /forms/1.json
  def show
  end

  # GET /forms/new
  def new
    @form = Form.new
  end

  # GET /forms/1/edit
  def edit
  end

  # POST /forms
  # POST /forms.json
  def create
    @form = Form.new(form_params)
    Mobility.with_locale(params[:lang]) do
      respond_to do |format|
        if @form.save
          format.html { redirect_to @form, notice: t('.notice') }
          format.json { render :show, status: :created, location: @form }
        else
          format.html { render :new, lang: params[:lang] }
          format.json { render json: @form.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # PATCH/PUT /forms/1
  # PATCH/PUT /forms/1.json
  def update
    Mobility.with_locale(params[:lang]) do
      respond_to do |format|
        if @form.update(form_params)
          format.html { redirect_to @form, notice: t('.notice') }
          format.json { render :show, status: :ok, location: @form }
        else
          format.html { render :edit, lang: params[:lang] }
          format.json { render json: @form.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # DELETE /forms/1
  # DELETE /forms/1.json
  def destroy
    @form.destroy
    respond_to do |format|
      format.html { redirect_to forms_url, notice: t('.notice') }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_form
      @form = Form.find(params[:id])
    end

    def has_power?
      unless current_user.use_form_builder?
        redirect_to forms_path, :flash => { :danger => t('access_denied') }
      end
    end

    def usable?
      unless current_user.use_forms?
        redirect_to root_path, :flash => { :danger => t('access_denied') }
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def form_params
      params.require(:form).permit(:title, :domain_id, :submit_button, form_fields_attributes: FormField.attribute_names.map(&:to_sym).push(:tex).push(:_destroy).push(field_options_attributes: FieldOption.attribute_names.map(&:to_sym).push(:_destroy)))
    end
end
