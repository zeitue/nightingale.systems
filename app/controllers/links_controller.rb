class LinksController < ApplicationController
  before_action :set_link, only: [:show, :edit, :update, :destroy]
  before_action :has_power?, only: [:new, :edit, :create, :update, :destroy]
  before_action :usable?
  # GET /links
  # GET /links.json
  def index
    case current_user.account_type
    when 'superuser'
      @links = Link.all.order('title DESC').page(params[:page]).per(8)
    when ''
      @links = Link.where('(account_type = 0 or account_type = 1) and domain_id = ?', current_user.domain_id).order('title DESC').page(params[:page]).per(8)
    when 'student'
      @links = Link.where('(account_type = 0 or account_type = 1) and domain_id = ?', current_user.domain_id).order('title DESC').page(params[:page]).per(8)
    when 'preceptor'
      @links = Link.where('(account_type = 0 or account_type = 2) and domain_id = ?', current_user.domain_id).order('title DESC').page(params[:page]).per(8)
    when 'administrator'
      @links = Link.where('domain_id = ?', current_user.domain_id).order('title DESC').page(params[:page]).per(8)
    when 'faculty'
      @links = Link.where('domain_id = ?', current_user.domain_id).order('title DESC').page(params[:page]).per(8)
    end
  end

  # GET /links/1
  # GET /links/1.json
  def show
  end

  # GET /links/new
  def new
    @link = Link.new
  end

  # GET /links/1/edit
  def edit
  end

  # POST /links
  # POST /links.json
  def create
    @link = Link.new(link_params)
    Mobility.with_locale(params[:lang]) do
      respond_to do |format|
        if @link.save
          format.html { redirect_to @link, notice: t('.notice')}
          format.json { render :show, status: :created, location: @link }
        else
          format.html { render :new, lang: params[:lang] }
          format.json { render json: @link.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # PATCH/PUT /links/1
  # PATCH/PUT /links/1.json
  def update
    Mobility.with_locale(params[:lang]) do
      respond_to do |format|
        if @link.update(link_params)
          format.html { redirect_to @link, notice: t('.notice') }
          format.json { render :show, status: :ok, location: @link }
        else
          format.html { render :edit, lang: params[:lang] }
          format.json { render json: @link.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # DELETE /links/1
  # DELETE /links/1.json
  def destroy
    @link.destroy
    respond_to do |format|
      format.html { redirect_to links_url, notice: t('.notice') }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_link
      @link = Link.find(params[:id])
    end

    def has_power?
      if !current_user.links_create_edit?
        redirect_to links_path, :flash => { :danger => t('access_denied') }
      end
    end

    def usable?
      unless current_user.use_links?
        redirect_to root_path, :flash => { :danger => t('access_denied') }
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def link_params
      params.require(:link).permit(:title, :url, :account_type, :domain_id)
    end
end
