class LocationsController < ApplicationController
  before_action :set_location, only: [:show, :edit, :update, :destroy]
  #before_action :has_power?, only: [:new, :edit, :create, :update, :destroy]
  before_action :usable?
  # GET /locations
  # GET /locations.json
  def index
    if current_user.superuser?
      @locations = Location.all.order('name DESC').page(params[:page]).per(8)
    else
      @locations = Location.domained(current_user).order('name DESC').page(params[:page]).per(8)
    end
  end

  # GET /locations/1
  # GET /locations/1.json
  def show
  end

  # GET /locations/new
  def new
    @location = Location.new
  end

  # GET /locations/1/edit
  def edit
  end

  # POST /locations
  # POST /locations.json
  def create
    @location = Location.new(location_params)
    Mobility.with_locale(params[:lang]) do
      respond_to do |format|
        if @location.save
          format.html { redirect_to @location, notice: t('.notice') }
          format.json { render :show, status: :created, location: @location }
        else
          format.html { render :new, lang: params[:lang] }
          format.json { render json: @location.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # PATCH/PUT /locations/1
  # PATCH/PUT /locations/1.json
  def update
    Mobility.with_locale(params[:lang]) do
      respond_to do |format|
        if @location.update(location_params)
          format.html { redirect_to @location, notice: t('.notice') }
          format.json { render :show, status: :ok, location: @location }
        else
          format.html { render :edit, lang: params[:lang] }
          format.json { render json: @location.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # DELETE /locations/1
  # DELETE /locations/1.json
  def destroy
    @location.destroy
    respond_to do |format|
      format.html { redirect_to locations_url, notice: t('.notice') }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_location
      @location = Location.find(params[:id])
    end

    def has_power?
      if !current_user.has_power?
        redirect_to locations_path, :flash => { :danger => t('access_denied') }
      end
    end

    def usable?
      unless current_user.use_locations?
        redirect_to root_path, :flash => { :danger => t('access_denied') }
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def location_params
      params.require(:location).permit(:address_line1, :address_line2, :country, :region, :city, :zip, :name, :unit, :domain_id)
    end
end
