class MailboxController < ApplicationController
  before_action :usable?
  def index
    redirect_to mailbox_inbox_path
  end

  def inbox
    @inbox = mailbox.inbox.page(params[:page]).per(8)
    @active = :inbox
  end

  def sent
    @sent = mailbox.sentbox.page(params[:page]).per(8)
    @active = :sent
  end

  def trash
    @trash = mailbox.trash.page(params[:page]).per(8)
    @active = :trash
  end

  private
  def usable?
    unless current_user.use_mail?
      redirect_to root_path, :flash => { :danger => t('access_denied') }
    end
  end
end
