class MasteriesController < ApplicationController
  before_action :set_mastery, only: [:show, :edit, :update, :destroy]
  before_action :usable?

  # GET /masteries
  # GET /masteries.json
  def index
    if current_user.superuser?
      @masteries = Mastery.all.page(params[:page]).per(8)
    else
      @masteries = Mastery.domained(current_user).page(params[:page]).per(8)
    end
  end

  # GET /masteries/1
  # GET /masteries/1.json
  def show
  end

  # GET /masteries/new
  def new
    @mastery = Mastery.new
  end

  # GET /masteries/1/edit
  def edit
  end

  # POST /masteries
  # POST /masteries.json
  def create
    @mastery = Mastery.new(mastery_params)
    Mobility.with_locale(params[:lang]) do
      respond_to do |format|
        if @mastery.save
          format.html { redirect_to @mastery, notice: t('.notice') }
          format.json { render :show, status: :created, location: @mastery }
        else
          format.html { render :new, lang: params[:lang] }
          format.json { render json: @mastery.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # PATCH/PUT /masteries/1
  # PATCH/PUT /masteries/1.json
  def update
    Mobility.with_locale(params[:lang]) do
      respond_to do |format|
        if @mastery.update(mastery_params)
          format.html { redirect_to @mastery, notice: t('.notice') }
          format.json { render :show, status: :ok, location: @mastery }
        else
          format.html { render :edit, lang: params[:lang] }
          format.json { render json: @mastery.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # DELETE /masteries/1
  # DELETE /masteries/1.json
  def destroy
    @mastery.destroy
    respond_to do |format|
      format.html { redirect_to masteries_url, notice: t('.notice') }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_mastery
      @mastery = Mastery.find(params[:id])
    end

    def usable?
      unless current_user.use_mastery_builder?
        redirect_to root_path, :flash => { :danger => t('access_denied') }
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def mastery_params
      params.require(:mastery).permit(:title, :domain_id, abilityset_ids: [], abilitysets_attributes: [:id, :title, :level, :domain_id])
    end
end
