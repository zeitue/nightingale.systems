class RolesController < ApplicationController
  before_action :set_role, only: [:show, :edit, :update, :destroy]
  before_action :owns?, only: [:edit, :update, :destroy]
  before_action :admin?

  # GET /roles
  # GET /roles.json
  def index
    if current_user.superuser?
      @roles = Role.all.page(params[:page]).per(8)
    else
      @roles = Role.domained(current_user).page(params[:page]).per(8)
    end
  end

  # GET /roles/1
  # GET /roles/1.json
  def show
  end

  # GET /roles/new
  def new
    @role = Role.new
  end

  # GET /roles/1/edit
  def edit
  end

  # POST /roles
  # POST /roles.json
  def create
    @role = Role.new(role_params)
    Mobility.with_locale(params[:lang]) do
      respond_to do |format|
        if @role.save
          format.html { redirect_to @role, notice: t('.notice') }
          format.json { render :show, status: :created, location: @role }
        else
          format.html { render :new }
          format.json { render json: @role.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # PATCH/PUT /roles/1
  # PATCH/PUT /roles/1.json
  def update
    Mobility.with_locale(params[:lang]) do
      respond_to do |format|
        if @role.update(role_params)
          format.html { redirect_to @role, notice: t('.notice') }
          format.json { render :show, status: :ok, location: @role }
        else
          format.html { render :edit }
          format.json { render json: @role.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # DELETE /roles/1
  # DELETE /roles/1.json
  def destroy
    @role.destroy
    respond_to do |format|
      format.html { redirect_to roles_url, notice: t('.notice') }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_role
      @role = Role.find(params[:id])
    end
    def admin?
      if !current_user.admin?
        redirect_to root_path, :flash => { :danger => t('access_denied') }
      end
    end

    def owns?
      unless (current_user.admin? && @role.domain_id == current_user.domain_id) || current_user.super_user?
        redirect_to root_path, :flash => { :danger => t('access_denied') }
      end
    end
    # Never trust parameters from the scary internet, only allow the white list through.
    def role_params
      params.require(:role).permit(:title, :use_events, :events_has_calendar, :events_overseer, :events_has_tasks, :events_has_log, :events_create_edit, :events_sign_in, :use_notes, :use_mail, :use_user_directory, :use_links, :links_create_edit, :use_locations, :use_mastery_builder, :has_mastery, :mastery_rank_up, :use_form_builder, :view_submitted_forms, :domain_id, :use_forms, role_forms_attributes: RoleForm.attribute_names.map(&:to_sym).push(:tex).push(:_destroy))
    end
end
