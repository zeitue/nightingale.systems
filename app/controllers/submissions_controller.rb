class SubmissionsController < ApplicationController
  #before_action :set_submission, only: [:show, :edit, :update, :destroy]
  before_action :load_form
  before_action :load_participant
  after_action :update_participant, only: [:create]
  before_action :usable?

  # GET /submissions
  # GET /submissions.json
  def index
    if current_user.student? || current_user.preceptor?
      @submissions = @form.submissions.where(user_id: current_user.id).page(params[:page]).per(8)
    else
      @submissions = @form.submissions.all.page(params[:page]).per(8)
    end
  end

  # GET /submissions/1
  # GET /submissions/1.json
  def show
    @submission = @form.submissions.find(params[:id])
    viewable?(@submission)
  end

  # GET /submissions/new
  def new
    @submission = @form.submissions.new
    @form.fields.each do |ff|
      text_response = @submission.text_responses.build(form_field_id: ff.id)
    end
  end

  # GET /submissions/1/edit
  def edit
    @submission = @form.submissions.find(params[:id])
    has_permission?(@submission)
  end

  # POST /submissions
  # POST /submissions.json
  def create
    @submission = @form.submissions.new(submission_params)

    respond_to do |format|
      if @submission.save
        format.html { redirect_to [@form, @submission], notice: t('.notice', form: @form.identifier) }
        format.json { render :show, status: :created, location: @submission }
      else
        format.html { render :new, participant: params[:participant] }
        format.json { render json: @submission.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /submissions/1
  # PATCH/PUT /submissions/1.json
  def update
    @submission = @form.submissions.find(params[:id])
    has_permission?(@submission)
    respond_to do |format|
      if @submission.update(submission_params)
        format.html { redirect_to [@form, @submission], notice: t('.notice', form: @form.identifier) }
        format.json { render :show, status: :ok, location: @submission }
      else
        format.html { render :edit }
        format.json { render json: @submission.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /submissions/1
  # DELETE /submissions/1.json
  def destroy
    @submission = @form.submissions.find(params[:id])
    if destroyable?(@submission)
      @submission.destroy
      respond_to do |format|
        format.html { redirect_to form_submissions_path(@form), notice: t('.notice', form: @form.identifier) }
        format.json { head :no_content }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    # def set_submission
    #   @submission = Submission.find(params[:id])
    # end

    def load_form
      @form = Form.find(params[:form_id])
    end

    def load_participant
      if params[:participant]
        @participant = Participant.find(params[:participant])
      end
    end

    def update_participant
      if @participant
        ParticipantSubmission.create(participant_id: @participant.id, submission_id: @submission.id)
        @participant.check_complete?
      end
    end


    def has_permission?(submission)
      unless current_user.admin? || (current_user.faculty? && !submission.user.has_power?) || submission.user_id == current_user.id
        redirect_to form_submissions_path(@form), :flash => { :danger => t('access_denied') }
      end
    end

    def viewable?(submission)
      unless current_user.admin? || current_user.faculty? || submission.user_id == current_user.id
        redirect_to form_submissions_path(@form), :flash => { :danger => t('access_denied') }
      end
    end

    def destroyable?(submission)
      unless current_user.admin? || (current_user.faculty? && !submission.user.has_power?) || (submission.user_id == current_user.id && current_user.has_power?)
        redirect_to form_submissions_path(@form), :flash => { :danger => t('access_denied') }
        false
      else
        true
      end
    end
    def usable?
      unless current_user.use_forms?
        redirect_to root_path, :flash => { :danger => t('access_denied') }
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def submission_params
      params.require(:submission).permit(:form_id, :user_id, :submitted_at, text_responses_attributes: TextResponse.attribute_names.map(&:to_sym).push(:_destroy))
    end
end
