class UsersController < ApplicationController
  before_action :set_user, only: [:update_mastery, :mastery, :show, :edit, :update, :destroy, :activate, :deactivate, :information, :contacts]
  before_action :admin?, only: [:new, :edit, :create, :destroy, :active, :deactivate]
  before_action :usable?
  before_action :has_mastery, only: [:mastery]
  def mastery
    @knows = @user.knows
  end


  def contacts
  end

  def update_mastery
    if current_user.mastery_rank_up?
      knows = Know.where(id: params[:know_ids])
      knows.update_all('proficiency = proficiency + 1')
      if knows.collect {|k| k.ability.proficiency_level <= k.proficiency}.all?
        @user.level_up!
      end
      flash[:notice] = t('.notice')
      redirect_to mastery_user_path
    else
      flash[:error] = t('.error')
      redirect_to mastery_user_path
    end
  end

  def index
    if current_user.superuser?
      if params[:search] && !params[:search].blank?
        @users = User.search(params[:search]).order("created_at DESC").page(params[:page]).per(8)
      else
        @users = User.all.order("created_at DESC").page(params[:page]).per(8)
      end
    else
      if params[:search] && !params[:search].blank?
        @users = User.domained(current_user).search(params[:search]).order("created_at DESC").page(params[:page]).per(8)
      else
        @users = User.domained(current_user).order("created_at DESC").page(params[:page]).per(8)
      end
    end
  end

  def show
  end

  def new
    @user = User.new
  end

  def edit
    super_safe
  end

  def create
    @user = User.new(user_params)
    @user.password = @user.birthdate.nil? ? Date.new(1970, 1, 1).strftime('%F') : @user.birthdate.strftime('%F')
    @user.skip_confirmation!
    @user.active = true
    if @user.save
      redirect_to @user, notice: t('.notice')
    else
      render :new
    end
  end

  def update
    super_safe
    if @user.update(user_params)
      respond_to do |format|
        format.html { redirect_to @user, notice: t('.notice') }
        format.js {render inline: "location.reload();" }
      end
    else
      render :edit
    end
  end

  def destroy
    if @user.destroy
      redirect_to users_path, notice: t('.notice')
    end
  end

  def activate
    if @user.update(active: true)
      redirect_back(fallback_location: users_path, notice: t('.notice'))
    end
  end

  def deactivate
    if @user.update(active: false)
      redirect_back(fallback_location: users_path, notice: t('.notice'))
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_user
    @user = User.find(params[:id])
  end

  def admin?
    if !current_user.admin?
      redirect_to users_path, :flash => { :danger => t('access_denied') }
    end
  end

  def super_safe
    if @user.superuser? && !current_user.superuser?
      redirect_to users_path, :flash => { :danger => t('access_denied') }
    end
  end

  def has_mastery
    unless @user.has_mastery?
      redirect_to users_path, :flash => { :danger => t('.no_mastery') }
    end
  end

  def usable?
    unless current_user.use_user_directory?
      redirect_to root_path, :flash => { :danger => t('access_denied') }
    end
  end
  # Only allow a trusted parameter "white list" through.
  def user_params
    params.require(:user).permit(:prefix, :first_name, :middle_name, :last_name, :suffix, :language, :account_type, :extid, :level, :timezone, :email, :birthdate, :mastery_id, :domain_id, :role_id, {avatars: []}, :avatars_cache
)
  end
end
