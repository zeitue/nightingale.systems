module ApplicationHelper

  def available_locales
    hidden = [:zh]
    (I18n.available_locales - hidden)
  end

  def locale_name_pairs
    available_locales.map do |locale|
      [I18n.t(:language, :locale => locale.to_s).to_s, locale.to_s]
    end
  end

  def create_links
    html=""
    locale_name_pairs.each do |link|
      html += content_tag(:li, link_to("#{link.first}", url_for(request.params.merge(:locale => link.last))))
    end
    html.html_safe
  end

  def trash_button(object)
    link_to icon('trash'), object, method: :delete, data: { confirm: t('devise.registrations.edit.are_you_sure') }, class: 'btn btn-default'
  end

  def active_page(active_page)
    @active == active_page ? " active" : ""
  end
  def code2language(code)
    I18n.t(:language, :locale => code)
  end

  def title
    if content_for?(:title)
      content_for :title
    elsif "#{ controller_path.tr('/', '.') }".include?('devise') || ['create','update'].any? {|e| action_name.include? e}

      I18n.t('nightingale')
    else
      I18n.t('nightingale') + ' | ' + I18n.t("#{ controller_path.tr('/', '.') }.#{ action_name }.title", default: :site_name)
    end

  end
end
