module DomainsHelper

  def map_providers
    [[t('.google_maps'), 'google_maps'],
     [t('.bing_maps'), 'bing_maps'],
     [t('.yandex_maps'), 'yandex_maps'],
     [t('.baidu_maps'), 'baidu_maps']]
  end

  def student_form_fill_options
    [[t('.no'), 'no'],
     [t('.yes'), 'yes'],
     [t('.before_logout'), 'before_logout']]
  end

  def overseer_form_fill_options
    [[t('.no'), 'no'],
     [t('.yes'), 'yes'],
     [t('.before_logout'), 'before_logout']]
  end

  def overseer_agreement_form_fill_options
    [[t('.no'), 'no'],
     [t('.per_event'), 'per_event'],
     [t('.until_expired'), 'until_expired']]
  end
end
