module EventsHelper


  def within(e, h)
    start = e.start.utc
    stop = e.stop.utc
    now = DateTime.now.utc
    margin = (e && e.domain && e.domain.time_margin) ? e.domain.time_margin.to_i.minutes : 15.minutes
    result = 'default'
    # logged in on time
    if h.login && (start + margin) > h.login && h.login > (start - margin)
      result = 'success'

      # logout on time
      if h.logout && (stop + margin) > h.logout && h.logout > (stop - margin)
        result = 'success'
      # logged out but late
      elsif h.logout && !((stop + margin) > h.logout && h.logout > (stop - margin))
        result = 'danger'
      # not logged out and safe to log out
      elsif h.logout.nil? && (stop + margin) > now && now > (stop - margin)
        result = 'warning'
      # not logged out and late
      elsif h.logout.nil? && (stop + margin) < now
        result = 'danger'
      end
    # logged in but late
    elsif h.login && !((start + margin) > h.login && h.login > (start - margin))
      result = 'danger'
    # not logged in and safe to log in
    elsif h.login.nil? && (start + margin) > now && now > (start - margin)
      result = 'warning'
    # not logged in and late
    elsif h.login.nil? && (start + margin) < now
      result = 'danger'
    end

    result
  end


end
