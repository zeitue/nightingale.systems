class FallbacksDisabledFormBuilder < ActionView::Helpers::FormBuilder
  %w[text_field text_area].each do |field_name|
    define_method field_name do |attribute, options={}|
      if @object.translated_attribute_names.include?(attribute)
        super(attribute, options.merge(value: @object.send(attribute, fallback: false)))
      else
        super(attribute, options)
      end
    end
  end
end
