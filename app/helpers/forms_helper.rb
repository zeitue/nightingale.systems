module FormsHelper
  def options_for_form_type
    [[t('.text'), 'text'], [t('.textarea'), 'textarea'], [t('.date'), 'date'],
     [t('.datetime'), 'datetime'], [t('.number'), 'number'],
     [t('.select'), 'select'], [t('.checkbox'), 'checkbox'],
     [t('.radio'), 'radio'], [t('.user_select'), 'user_select'], [t('.student_select'), 'student_select'],
     [t('.preceptor_select'), 'preceptor_select'], [t('.faculty_select'), 'faculty_select'],
     [t('.administrator_select'), 'administrator_select'], [t('.overseer_select'), 'overseer_select'],
     [t('.abilityset_select'), 'abilityset_select'],
     [t('.agreement_expiration'), 'agreement_expiration']]
  end
end
