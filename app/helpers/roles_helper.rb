module RolesHelper
    def form_usage
    [[t('.per_event_agreement'), 'per_event_agreement'],
     [t('.until_expired_agreement'), 'until_expired_agreement'],
     [t('.event_form'), 'event_form']]
  end
end
