module UsersHelper
  def avatar(user)
    user.avatars.empty? ? 'default.png' : user.avatars.last.url
  end

  # def avatar_xxhdpi(user)
  #   user.avatars.empty? ? 'xxhdpi_default.png' : user.avatars.last.xxhdpi.url
  # end

  def avatar_thumb(user)
    user.avatars.empty? ? 'ldpi_default.png' : user.avatars.last.ldpi.url
  end
end
