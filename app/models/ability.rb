class Ability < ApplicationRecord
  include Mobility
  has_many :abilitysetabilities
  has_many :abilitysets, through: :abilitysetabilities
  has_many :knows
  has_many :users,  -> { distinct }, through: :knows
  accepts_nested_attributes_for :abilitysets, reject_if: :reject_abilityset
  validates :title, uniqueness: { scope: [:category, :required_level, :domain_id] }
  validates :category, uniqueness: { scope: [:title, :required_level, :domain_id] }
  validates :required_level, uniqueness: { scope: [:title, :category, :domain_id] }
  validates_presence_of :title
  validates_presence_of :category
  validates_presence_of :required_level
  validates_presence_of :proficiency_level
  translates :title, fallbacks: true, locale_accessors: true
  translates :category, fallbacks: true, locale_accessors: true
  before_save :clean_up
  belongs_to :domain
  scope :domained, ->(u) { where("abilities.domain_id = ?", u.domain_id) if u.domain_id.present? }

  def identifier
    '/' + category_l.to_s + '/' + required_level.to_s + '/' + title_l.to_s
  end

  def complete?(user_id)
    know = self.knows.where(user_id: user_id).first
    if know
      know.proficiency.to_i >= self.proficiency_level.to_i
    else
      false
    end
  end

  def title_l
    self.title || self.title(fallback: read_attribute(:title).keys.first)
  end

  def category_l
    self.category || self.category(fallback: read_attribute(:category).keys.first)
  end

  private

  def clean_up
    self.title = self.title.strip
    self.category = self.category.strip
  end

  def reject_abilityset(a)
    a['title'].blank? && a['level'].blank?
  end
end
