class Abilityset < ApplicationRecord
  include Mobility
  has_many :masteryabilitysets
  has_many :masteries, through: :masteryabilitysets
  has_many :abilitysetabilities
  has_many :abilities, through: :abilitysetabilities
  has_many :events
  has_many :abilityset_submissions
  has_many :submissions, through: :abilityset_submissions
  validates_presence_of :title
  validates_presence_of :level
  validates :title, uniqueness: { scope: [:level, :domain_id] }
  validates :level, uniqueness: { scope: [:title, :domain_id] }
  accepts_nested_attributes_for :abilities, reject_if: :reject_ability
  accepts_nested_attributes_for :masteries, reject_if: :reject_mastery
  before_save :clean_up
  belongs_to :domain
  translates :title, fallbacks: true, locale_accessors: true
  scope :domained, ->(u) { where("abilitysets.domain_id = ?", u.domain_id) if u.domain_id.present? }

  def identifier
    '/' + self.level.to_s + '/' + self.title_l.to_s
  end

  def complete?(user_id)
    self.abilities.collect do |ability|
      ability.complete?(user_id)
    end.all?
  end

  def title_l
    self.title || self.title(fallback: read_attribute(:title).keys.first)
  end

  private

  def clean_up
    self.title = self.title.strip
  end


  def reject_mastery(a)
    a['title'].blank?
  end

  def reject_ability(a)
    a['title'].blank? && a['category'] && a['required_level'] && a['proficiency_level']
  end
end
