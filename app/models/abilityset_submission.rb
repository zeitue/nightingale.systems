class AbilitysetSubmission < ApplicationRecord
  belongs_to :abilityset
  belongs_to :submission
end
