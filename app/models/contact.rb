class Contact < ApplicationRecord
  belongs_to :user
  validates :service, presence: true
  validates :identity, presence: true
  before_save :clean_up

  def to_button
    case service
    when 'phone'
      make_link("tel:#{identity}", 'phone')
    when 'mobile'
      make_link("tel:#{identity}", 'mobile')
    when 'email'
      make_link("mailto:#{identity}", 'envelope-square')
    when 'fax'
      make_link("fax:#{identity}", 'fax')
    when 'skype'
      make_link("skype:#{identity}", 'skype')
    when 'wechat'
      make_link("weixin://dl/chat?#{identity}", 'weixin')
    end
  end

  private

  def make_link(link, icon)
    "<a class='btn btn-default btn-block' href='#{link}'><i class='fa fa-#{icon}'></i>&nbsp;#{identity}</a>".html_safe
  end

  def clean_up
    self.identity = self.identity.to_s.strip
  end

end

