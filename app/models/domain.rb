class Domain < ApplicationRecord
  include Mobility
  validates :name, presence: true
  validates :namespace, presence: true
  validates :language, presence: true
  validates :timezone, presence: true
  validates :country, presence: true
  validates :student_starting_level, presence: true
  validates :namespace, uniqueness: true
  validates :time_margin, presence: true
  translates :name, fallbacks: true, locale_accessors: true
  has_many :users
  has_many :events
  has_many :links
  has_many :locations
  has_many :abilities
  has_many :abilitysets
  has_many :masteries
  has_many :forms
  before_save :clean_up

  def name_l
    self.name || self.name(fallback: read_attribute(:name).keys.first)
  end


  def identifier
    if self.name_l.to_s.blank?
      self.namespace
    else
      "#{self.name_l.to_s} - #{self.namespace}"
    end
 end
 def clean_up
   self.name = self.name.to_s.strip
   self.namespace = self.namespace.to_s.strip
 end

end
