class Event < ApplicationRecord
  #belongs_to :overseer, class_name: 'User', foreign_key: :overseer_id
  #belongs_to :student, class_name: 'User', foreign_key: :student_id
  belongs_to :location
  belongs_to :abilityset
  belongs_to :domain
  belongs_to :user
  #belongs_to :student_evaluation, class_name: 'Submission', foreign_key: 'student_evaluation_id', optional: true
  #belongs_to :overseer_evaluation, class_name: 'Submission', foreign_key: 'overseer_evaluation_id', optional: true
  #belongs_to :overseer_agreement, class_name: 'Submission', foreign_key: 'overseer_agreement_id', optional: true
  validates_presence_of :start
  validates_presence_of :stop
  scope :domained, ->(u) { where("events.domain_id = ?", u.domain_id) if u.domain_id.present? }
  #before_save :link_agreement
  has_many :participants
  has_many :users, through: :participants
  accepts_nested_attributes_for :participants, reject_if: :all_blank, allow_destroy: true

  def part_of?(user)
    self.participants.exists?(user_id: user.id)
  end

  def start_time
    start
  end




  def link_agreement
    if self.overseer_agreement.nil? && self.domain &&
       self.domain.overseer_agreement_form_fill.to_s == 'until_expired'  &&
       self.domain.overseer_agreement_form

      party_ids = UserSubmission.where(user_id: [overseer_id]).distinct.pluck(:submission_id) &
                  UserSubmission.where(user_id: [student_id]).distinct.pluck(:submission_id)

      possible =  Submission.where(form_id: self.domain.overseer_agreement_form_id, id: party_ids)
                    .joins(:form_fields).where(form_fields: { field_type: 'agreement_expiration'})
                    .joins(:text_responses).where("to_date(text_responses.answer, 'YYYY MM DD') > ?", self.stop.to_date).distinct

      with_ability = possible.joins(:abilityset_submissions).where(abilityset_submissions: {abilityset_id: self.abilityset_id}).distinct

      self.overseer_agreement = with_ability.count == 0 ? possible.first : with_ability.first
    end
  end

end
