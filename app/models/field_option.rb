class FieldOption < ApplicationRecord
  include Mobility
  belongs_to :form_field
  translates :option_key, fallbacks: true, locale_accessors: true
  before_save :clean_up

  def option_key_l
    self.option_key || self.option_key(fallback: read_attribute(:option_key).keys.first)
  end

  def clean_up
    self.option_key = self.option_key.to_s.strip
    self.option_value = self.option_value.to_s.strip
  end
end
