class Form < ApplicationRecord
  include Mobility
  belongs_to :domain
  has_many :form_fields, dependent: :destroy
  accepts_nested_attributes_for :form_fields, reject_if: proc {|a| a['position'].blank?}, allow_destroy: true
  translates :title, fallbacks: true, locale_accessors: true
  translates :submit_button, fallbacks: true, locale_accessors: true
  validates :title, presence: true
  validates :submit_button, presence: true
  has_many :submissions
  has_many :domains
  validate :check_expire_count
  scope :domained, ->(u) { where("forms.domain_id = ?", u.domain_id) if u.domain_id.present? }
  before_save :clean_up

  def fields
    form_fields.order('form_fields.position')
  end

  def title_l
    self.title || self.title(fallback: read_attribute(:title).keys.first)
  end

  def submit_l
    self.submit_button || self.submit_button(fallback: read_attribute(:submit_button).keys.first)
  end

  def identifier
    title_l
  end

  def check_expire_count
    if self.form_fields.reject(&:marked_for_destruction?).collect(&:field_type).count{|f| f == 'agreement_expiration'} > 1
      self.errors.add :base, I18n.t('only_one_agreement_expiration', scope: [:form, :messages])
    end
  end

  def clean_up
    self.title = self.title.to_s.strip
    self.submit_button = self.submit_button.to_s.strip
  end

end
