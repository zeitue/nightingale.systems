class FormField < ApplicationRecord
  include Mobility
  belongs_to :form
  has_many :field_options, dependent: :destroy
  has_many :text_responses
  accepts_nested_attributes_for :field_options, reject_if: proc { |a| a['option_value'].blank? } , allow_destroy: true
  #before_validation :mark_for_destruction
  translates :label, fallbacks: true, locale_accessors: true
  translates :placeholder, fallbacks: true, locale_accessors: true
  translates :tooltip, fallbacks: true, locale_accessors: true
  translates :tex, fallbacks: false, locale_accessors: true
  translates :text, fallbacks: true, locale_accessors: true
  before_save :clean_up

  #def mark_for_destruction
  #  field_options.each do |field_option|
  #    if field_option.option_key.blank? || field_option.option_value.blank? ||
  #       (self.field_type.to_s != 'radio' && self.field_type.to_s != 'select')
  #      field_option.mark_for_destruction
  #    end
  #  end
  #end
  before_save :set_required
  before_save :mark_children_for_removal

  def mark_children_for_removal
    self.field_options.each do |child|
      child.mark_for_destruction if child.option_value.blank? || (self.field_type.to_s != 'radio' && self.field_type.to_s != 'select')
    end
  end

  def set_required
    if self.field_type == 'agreement_expiration'
      self.required = true
    end
  end

  def label_l
    self.label || self.label(fallback: read_attribute(:label).keys.first)
  end

  def placeholder_l
    self.placeholder || self.placeholder(fallback: read_attribute(:placeholder).keys.first)
  end

  def tooltip_l
    self.tooltip || self.tooltip(fallback: read_attribute(:tooltip).keys.first)
  end

  def text_l
    self.text || self.text(fallback: read_attribute(:text).keys.first)
  end

  # a little bit of genius
  def tex
    self.text(fallback: false)
  end

  def tex=(value)
    self.text = value
  end

  def clean_up
    self.label = self.label.to_s.strip
    self.placeholder = self.placeholder.to_s.strip
    self.tooltip = self.tooltip.to_s.strip
  end

end
