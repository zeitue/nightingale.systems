class Know < ApplicationRecord
  belongs_to :user
  belongs_to :ability

  def width
    if self.proficiency <= self.ability.proficiency_level
      (self.proficiency.to_f/self.ability.proficiency_level.to_f*100).to_i
    else
      100
    end
  end

  def value
    if self.proficiency <= self.ability.proficiency_level
      self.proficiency
    else
      100
    end
  end

  def bar
    if self.proficiency < self.ability.proficiency_level
      'progress-bar-default'
    else
      'progress-bar-success'
    end
  end

  def maxvalue
    self.ability.proficiency_level
  end

  def bartext
    "#{self.proficiency.to_s + '/' + self.ability.proficiency_level.to_s}"
  end
end
