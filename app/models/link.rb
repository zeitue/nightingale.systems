class Link < ApplicationRecord
  include Mobility
  validates :account_type, presence: true
  validates :title, presence: true
  validates :url, presence: true
  before_save :clean_up
  belongs_to :domain
  translates :title, fallbacks: true, locale_accessors: true
  scope :domained, ->(u) { where("links.domain_id = ?", u.domain_id) if u.domain_id.present? }

  def account_type?
    I18n.t(['all', 'student', 'preceptor', 'faculty', 'administrator', 'overseer'][self.account_type])
  end

  def title_l
    self.title || self.title(fallback: read_attribute(:title).keys.first)
  end

  def identifier
    title_l
  end

  private

  def clean_up
    self.title = self.title.strip
    self.url = self.url.strip
    self.url = 'http://' + self.url unless self.url.include?(':')
  end

end
