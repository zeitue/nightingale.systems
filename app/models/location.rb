class Location < ApplicationRecord
  include Mobility
  validates :name, presence: true
  validates :address_line1, presence: true
  validates :country, presence: true
  validates :region, presence: true
  validates :city, presence: true
  validates :zip, presence: true
  has_many :events
  before_save :clean_up
  belongs_to :domain
  translates :name, fallbacks: true, locale_accessors: true
  translates :address_line1, fallbacks: true, locale_accessors: true
  translates :address_line2, fallbacks: true, locale_accessors: true
  translates :region, fallbacks: true, locale_accessors: true
  translates :city, fallbacks: true, locale_accessors: true
  translates :zip, fallbacks: true, locale_accessors: true
  translates :unit, fallbacks: true, locale_accessors: true

  scope :domained, ->(u) { where("locations.domain_id = ?", u.domain_id) if u.domain_id.present? }


  def identifier
    buffer = ''
    buffer += self.name_l.to_s + ' ' unless self.name_l.to_s.empty?
    buffer += self.unit_l.to_s + ' @ ' unless self.unit_l.to_s.empty?
    buffer += self.address_line1_l.to_s + ' ' unless self.address_line1_l.to_s.empty?
    buffer += self.address_line2_l.to_s + ' ' unless self.address_line2_l.to_s.empty?
    buffer += self.city_l.to_s + ', ' unless city_l.to_s.empty?
    buffer += self.region_l.to_s + ' ' unless self.region_l.to_s.empty?
    buffer += country_name + ' ' unless self.country.to_s.empty?
    buffer += self.zip_l.to_s unless self.zip_l.to_s.empty?
    buffer
  end

  def country_name
    country = ISO3166::Country[self.country.to_s]
    country.translations[I18n.locale.to_s] || country.name
  end

  def navigate
    buffer = ''
    buffer += self.name_l.to_s + ' , ' unless self.name_l.to_s.empty?
    buffer += self.address_line1_l.to_s + ' ' unless self.address_line1_l.to_s.empty?
    buffer += self.address_line2_l.to_s + ' ' unless self.address_line2_l.to_s.empty?
    buffer += country_name + ' ' unless self.country.to_s.empty?
    buffer += self.city_l.to_s + ', ' unless city_l.to_s.empty?
    buffer += self.region_l.to_s + ' ' unless self.region_l.to_s.empty?
    buffer += self.zip_l.to_s unless self.zip_l.to_s.empty?
    if self.domain && self.domain.map_provider
      case self.domain.map_provider
      when 'google_maps'
        "https://www.google.com/maps/place/#{URI.encode(buffer)}"
      when 'baidu_maps'
        "http://api.map.baidu.com/geocoder?address=#{URI.encode(buffer)}&output=html"
      when 'yandex_maps'
        "http://maps.yandex.ru/?text#{URI.encode(buffer)}"
      when 'bing_maps'
        "https://www.bing.com/maps?where1=#{URI.encode(buffer)}"
      else
        "https://www.google.com/maps/place/#{URI.encode(buffer)}"
      end
    end
  end

  def name_l
    self.name || self.name(fallback: read_attribute(:name).keys.first)
  end

  def address_line1_l
    self.address_line1 || self.address_line1(fallback: read_attribute(:address_line1).keys.first)
  end

  def address_line2_l
    self.address_line2 || self.address_line2(fallback: read_attribute(:address_line2).keys.first)
  end

  def region_l
    self.region || self.region(fallback: read_attribute(:region).keys.first)
  end

  def city_l
    self.city || self.city(fallback: read_attribute(:city).keys.first)
  end

  def zip_l
    self.zip || self.zip(fallback: read_attribute(:zip).keys.first)
  end

  def unit_l
    self.unit || self.unit(fallback: read_attribute(:unit).keys.first)
  end

  private

  def clean_up
    self.name = self.name.to_s.strip
    self.address_line1 = self.address_line1.to_s.strip
    self.address_line2 = self.address_line2.to_s.strip
    self.region = self.region.to_s.strip
    self.city = self.city.to_s.strip
    self.zip = self.zip.to_s.strip
    self.unit = self.unit.to_s.strip
  end
end
