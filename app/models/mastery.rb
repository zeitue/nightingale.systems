class Mastery < ApplicationRecord
  include Mobility
  has_many :masteryabilitysets
  has_many :abilitysets, through: :masteryabilitysets
  validates_presence_of :title
  validates_uniqueness_of :title
  accepts_nested_attributes_for :abilitysets, reject_if: :reject_abilityset
  before_save :clean_up
  has_many :users
  belongs_to :domain
  translates :title, fallbacks: true, locale_accessors: true
  scope :domained, ->(u) { where("masteries.domain_id = ?", u.domain_id) if u.domain_id.present? }

  def identifier
    '/' + self.title_l.to_s
  end

  def get_levelup(user_id)
    result = 0
    self.abilitysets.group_by(&:level).sort.each do |level, abilityset_leveled|
      up = abilityset_leveled.collect do |abilityset|
        abilityset.complete?(user_id)
      end.all?
      result = level if up
    end
    result + 1
  end

  def title_l
    self.title || self.title(fallback: read_attribute(:title).keys.first)
  end

  private

  def clean_up
    self.title = self.title.strip
  end


  def reject_abilityset(a)
    a['title'].blank? && a['level'].blank?
  end
end
