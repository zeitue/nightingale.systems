class Note < ApplicationRecord
  belongs_to :user
  validates :user_id, presence: true
  validates :title, presence: true
  validates :body, presence: true
  before_save :clean_up

  private

  def clean_up
    self.title = self.title.strip
  end
end
