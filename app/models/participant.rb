class Participant < ApplicationRecord
  belongs_to :user
  belongs_to :role, optional: true
  belongs_to :event, dependent: :destroy, counter_cache: true
  before_save :set_role
  has_many :forms, through: :role
  has_many :participant_submissions
  has_many :submissions, through: :participant_submissions
  validates_uniqueness_of :user, :scope => :event
  validate :in_time
  def identifier
    self.user.identifier
  end

  def total_time
    if login && logout
      Time.at(logout - login).utc.strftime('%H:%M:%S')
    end
  end

  def filled(form)
    self.submissions.where(form_id: form.id).first
  end

  def check_complete?
    if !self.completed && ((self.login && self.logout) || self.user.events_sign_in?) && (self.forms.count == self.submissions.count)
      self.completed = true
      self.save
    else
      self.completed
    end
  end


  def in_time
    if self.event.new_record?
      e = (Event.domained(self.user).joins(:participants)
             .where('participants.user_id = ?', self.user.id)
             .where('(events.start between ? and ?) or (events.stop between ? and ?)', self.event.start, self.event.stop, self.event.start, self.event.stop).count > 0)
    else
      e = (Event.domained(self.user).joins(:participants)
             .where('participants.user_id = ?', self.user.id)
             .where('events.id != ? and ((events.start between ? and ?) or (events.stop between ? and ?))',self.event.id, self.event.start, self.event.stop, self.event.start, self.event.stop).count > 0)
    end
    if e
      self.errors.add :participant, I18n.t('error', scope: [:participants, :messages], user: self.user.identifier)
    end
  end

  private

  def set_role
    if self.user
      self.role_id = self.user.role_id
    end
  end

end

