class ParticipantSubmission < ApplicationRecord
  belongs_to :participant
  belongs_to :submission
end
