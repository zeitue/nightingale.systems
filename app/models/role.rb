class Role < ApplicationRecord
  include Mobility
  belongs_to :domain
  has_many :users
  has_many :role_forms, dependent: :destroy
  has_many :forms, through: :role_forms
  has_many :participants
  validates :title, presence: true
  translates :title, fallbacks: true, locale_accessors: true
  accepts_nested_attributes_for :role_forms, reject_if: :all_blank, allow_destroy: true
  scope :domained, ->(u) { where("roles.domain_id = ?", u.domain_id) if u.domain_id.present? }

  def identifier
    title_l
  end

  def title_l
    self.title || self.title(fallback: read_attribute(:title).keys.first)
  end
end
