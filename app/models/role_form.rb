class RoleForm < ApplicationRecord
  belongs_to :role
  belongs_to :form
  validates_uniqueness_of :form, :scope => :role
end
