class Submission < ApplicationRecord
  belongs_to :form
  belongs_to :user
  has_many :user_submissions, :dependent => :destroy
  has_many :abilityset_submissions, :dependent => :destroy
  has_many :text_responses, :dependent => :destroy
  has_many :form_fields, through: :text_responses
  has_many :users, through: :user_submissions
  has_many :abilitysets, through: :abilityset_submissions
  has_many :events, ->(submission) { unscope(where: :submission_id).where("events.student_evaluation_id = ? OR events.overseer_evaluation_id = ? OR events.overseer_agreement_id = ?", submission.id, submission.id, submission.id) }, class_name: 'Event'
  accepts_nested_attributes_for :text_responses, allow_destroy: true
  after_save :link_party
  after_save :link_abilitysets
  before_save :submit
  def responses
    self.text_responses.sort_by{|r| r.form_field.position.to_i}
  end


  def party_ids
    self.text_responses.joins(:form_field).where(:form_fields => {:field_type => ['user_select', 'student_select', 'administrator_select', 'preceptor_select', 'faculty_select', 'overseer_select']}).collect{|a| a.answer.to_i}
  end

  def link_party
    oids = self.users.ids
    pids = party_ids
    dids = oids - pids
    pids.each do |uid|
      self.user_submissions.find_or_create_by!(user_id: uid)
    end
    self.user_submissions.where(user_id: dids).destroy_all
  end

  def abilityset_ids
    self.text_responses.joins(:form_field).where(:form_fields => {:field_type => 'abilityset_select'}).collect{|a| a.answer.to_i}
  end

  def link_abilitysets
    oids = self.abilitysets.ids
    pids = abilityset_ids
    dids = oids - pids
    pids.each do |sid|
      self.abilityset_submissions.find_or_create_by!(abilityset_id: sid)
    end
    self.abilityset_submissions.where(abilityset_id: dids).destroy_all
  end

  def submit
    self.submitted_at ||= DateTime.now
  end
end
