class TextResponse < ApplicationRecord
  belongs_to :submission
  belongs_to :form_field
  validate  :response_check
  before_save :clean_up

  def response_check
    f = self.form_field
    s = [:form, :messages]

    # check for blank
    if f.required && self.answer.blank? && !(f.field_type.include?('select') || f.field_type == 'radio')
      if f.label_l.blank?
        errors.add(:text_response, I18n.t('.cannot_be_blank', scope: s))
      else
        errors.add(:text_response, I18n.t('.cannot_be_blank_field', scope: s, field: f.label_l))
      end
    end

    # checkbox
    if f.required && f.field_type == 'checkbox' && self.answer.to_i != 1
      if f.label_l.blank?
        errors.add(:text_response, I18n.t('.must_be_checked', scope: s))
      else
        errors.add(:text_response, I18n.t('.must_be_checked_field', scope: s, field: f.label_l))
      end
    end

    # select must have an option selected
    if f.required && (f.field_type.include?('select') || f.field_type == 'radio') && self.answer.blank?
      if f.label_l.blank?
        errors.add(:text_response, I18n.t('.choose_option', scope: s))
      else
        errors.add(:text_response, I18n.t('.choose_option_field', scope: s, field: f.label_l))
      end
    end
  end

  def clean_up
    self.answer = self.answer.to_s.strip
  end

end
