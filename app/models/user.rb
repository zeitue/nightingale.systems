class User < ApplicationRecord
  has_many :knows
  belongs_to :mastery, optional: true
  belongs_to :role, optional: true
  has_many :abilities,  -> { distinct }, through: :knows
  accepts_nested_attributes_for :knows, reject_if: :all_blank
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :recoverable,
         :rememberable, :trackable, :validatable, :confirmable, :lockable
  mount_uploaders :avatars, AvatarUploader
  has_many :notes
  has_many :contacts
  has_many :events
  has_many :user_submissions
  has_many :submissions, through: :user_submissions
  before_save :clean_up
  validate :check_demo
  validates_presence_of :email
  acts_as_messageable
  belongs_to :domain, optional: true
  scope :domained, ->(u) { where("users.domain_id = ?", u.domain_id) if u.domain_id.present? }
  scope :domained_students, ->(u) { where("users.domain_id = ? and users.account_type = ?", u.domain_id, "student") if u.domain_id.present? }
  scope :domained_preceptors, ->(u) { where("users.domain_id = ? and users.account_type = ?", u.domain_id, "preceptor") if u.domain_id.present? }
  scope :domained_faculty, ->(u) { where("users.domain_id = ? and users.account_type = ?", u.domain_id, "faculty") if u.domain_id.present? }
  scope :domained_administrators, ->(u) { where("users.domain_id = ? and users.account_type = ?", u.domain_id, "administrator") if u.domain_id.present? }
  scope :domained_overseers, ->(u) { where("users.domain_id = ? and (users.account_type = ? or users.account_type = ?)", u.domain_id, "faculty", "preceptor") if u.domain_id.present? }

  def name
    result = ''
    empty = false
    I18n.t('name.format.default').split('').each do |e|
      case e
      when 'P'
        if !self.prefix.empty?
          result += self.prefix
        else
          empty = true
        end
      when 'F'
        if !self.first_name.empty?
          result += self.first_name
        else
          empty = true
        end
      when 'M'
        if !self.middle_name.empty?
          result += self.middle_name
        else
          empty = true
        end
      when 'L'
        if !self.last_name.empty?
          result += self.last_name
        else
          empty = true
        end
      when 'S'
        if !self.suffix.empty?
          result += self.suffix
        else
          empty = true
        end
      else
        if !empty
          result += e
        else
          empty = false
        end
      end
    end
    result.strip
  end

  def identifier
    buffer = '/'
    buffer += self.domain.namespace if self.domain
    buffer += "/"
    buffer += self.role.identifier if self.role
    buffer += "/"
    buffer += "#{self.name} : " unless self.name.blank?
    buffer += "#{self.email}"
    buffer
  end

  def has_power?
    self.account_type.to_s == 'superuser' ||
    self.account_type.to_s == 'administrator' ||
    self.account_type.to_s == 'faculty'
  end

  def admin?
    self.account_type.to_s == 'administrator' ||
      self.account_type.to_s == 'superuser'
  end

  def superuser?
    self.account_type.to_s == 'superuser'
  end

  def overseer?
    self.account_type.to_s == 'preceptor' ||
    self.account_type.to_s == 'faculty' ||
    self.account_type.to_s == 'administrator' ||
    self.account_type.to_s == 'superuser'
  end

  def preceptor?
    self.account_type.to_s == 'preceptor'
  end

  def student?
    self.account_type.to_s == 'student'
  end

  def faculty?
    self.account_type.to_s == 'faculty'
  end

  def active_for_authentication?
    super and self.active
  end

  def inactive_message
    if !self.active
      I18n.t('devise.failure.inactive', locale: self.language)
    end
  end

  def account
    I18n.t(account_type.to_s.blank? ? 'undefined' : account_type)
  end

  def level_up!
    if self.mastery
      level_value = self.mastery.get_levelup(self.id)
      if level_value > self.level
        self.level = level_value
        self.save!
      end
    end
  end

  def use_events?
    self.role && self.role.use_events
  end

  def use_notes?
    self.role && self.role.use_notes
  end

  def events_has_calendar?
    self.role && self.role.events_has_calendar
  end

  def events_overseer?
    self.role && self.role.events_overseer
  end

  def events_has_tasks?
    self.role && self.role.events_has_tasks
  end

  def events_has_log?
    self.role && self.role.events_has_log
  end

  def events_create_edit?
    self.role && self.role.events_create_edit
  end

  def events_sign_in?
    self.role && self.role.events_sign_in
  end

  def use_mail?
    self.role && self.role.use_mail
  end

  def use_user_directory?
    self.admin? || (self.role && self.role.use_user_directory)
  end

  def use_links?
    self.role && self.role.use_links
  end

  def links_create_edit?
    self.role && self.role.links_create_edit
  end

  def use_locations?
    self.role && self.role.use_locations
  end

  def use_mastery_builder?
    self.role && self.role.use_mastery_builder
  end

  def has_mastery?
    self.role && self.role.has_mastery
  end

  def mastery_rank_up?
    self.role && self.role.mastery_rank_up
  end

  def use_form_builder?
    self.role && self.role.use_form_builder
  end

  def use_forms?
    self.role && self.role.use_forms
  end

  def view_submitted_forms?
    self.role && self.role.view_submitted_forms
  end



  def self.search(term)
    where('first_name ILIKE ANY (array[:terms]) or last_name ILIKE ANY (array[:terms]) or email ILIKE ANY (array[:terms])', terms: term.split.map {|val| "%#{val}%" })
  end

  private

  def clean_up

    # clean up fields
    self.prefix = self.prefix.to_s.strip
    self.first_name = self.first_name.to_s.strip
    self.middle_name = self.middle_name.to_s.strip
    self.last_name = self.last_name.to_s.strip
    self.suffix = self.suffix.to_s.strip
    self.email = self.email.downcase.to_s.strip
    self.extid = self.extid.to_s.strip
    self.birthdate = Date.new(1970, 1, 1) if self.birthdate.nil?
    # if they're a student set the level
    if self.student?
      self.level = 1 unless self.level.to_i > 0
    end

    # if they're a overseer make them a pin number
    #if self.overseer?
    #  self.pin = (0...6).map { (48 + rand(10)).chr }.join if self.pin.empty?
    #end
  end

  def check_demo
    if (self.domain && self.domain.demo) && User.domained_students(self).count >= 10
      errors.add :base, I18n.t('student_limit_exceeded', scope: [:domains, :messages])
    end

  end

  def mailboxer_email(object)
    self.email
  end

end
