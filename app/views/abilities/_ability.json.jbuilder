json.extract! ability, :id, :title, :category, :required_level, :created_at, :updated_at
json.url ability_url(ability, format: :json)
