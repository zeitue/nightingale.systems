json.extract! abilityset, :id, :title, :level, :created_at, :updated_at
json.url abilityset_url(abilityset, format: :json)
