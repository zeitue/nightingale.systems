json.extract! domain, :id, :name, :namespace, :language, :timezone, :country, :student_starting_level, :overseer_agreement, :created_at, :updated_at
json.url domain_url(domain, format: :json)
