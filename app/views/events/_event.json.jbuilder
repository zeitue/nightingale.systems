json.extract! event, :id, :overseer_id, :student_id, :location_id, :start, :stop, :login, :logout, :created_at, :updated_at
json.url event_url(event, format: :json)
