json.extract! link, :id, :title, :url, :account_type, :created_at, :updated_at
json.url link_url(link, format: :json)
