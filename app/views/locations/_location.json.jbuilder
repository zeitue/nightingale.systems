json.extract! location, :id, :address_line1, :address_line2, :country, :region, :city, :zip, :name, :unit, :created_at, :updated_at
json.url location_url(location, format: :json)
