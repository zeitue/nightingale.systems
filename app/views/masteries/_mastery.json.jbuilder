json.extract! mastery, :id, :title, :created_at, :updated_at
json.url mastery_url(mastery, format: :json)
