module ActionView
  module Helpers
    class FormBuilder

      def date_select(method, options = {}, html_options = {})
        locale = I18n.t('js.datetimepicker')
        existing_date = @object.send(method)
        formatted_date = existing_date.to_date.strftime("%F") if existing_date.present?
        @template.content_tag(:div, :class => "input-group", :id => "#{method}") do
          @template.content_tag(:span, @template.content_tag(:span, "", :class => "fa fa-calendar") ,:class => "input-group-addon") +
            @template.javascript_tag("  $(function () {$('##{method}').datetimepicker({locale: '#{locale}',
             icons: { time: 'fa fa-clock-o', date: 'fa fa-calendar', up: 'fa fa-arrow-up', down: 'fa fa-arrow-down'}});});") +
            text_field(method, :value => formatted_date, :class => "form-control datepicker input", :"data-date-format" => "YYYY-MM-DD")
        end
      end

      def datetime_select(method, options = {}, html_options = {})
        locale = I18n.t('js.datetimepicker')
        existing_time = @object.send(method)
        formatted_time = existing_time.to_time.strftime("%F %H:%M") if existing_time.present?
        @template.content_tag(:div, :class => "input-group", :id => "#{method}") do
          @template.content_tag(:span, @template.content_tag(:span, "", :class => "fa fa-calendar") ,:class => "input-group-addon") +
            @template.javascript_tag("  $(function () {$('##{method}').datetimepicker({locale: '#{locale}',
             icons: { time: 'fa fa-clock-o', date: 'fa fa-calendar', up: 'fa fa-arrow-up', down: 'fa fa-arrow-down'}});});") +
            text_field(method, :value => formatted_time, :class => "form-control datetimepicker input", :"data-date-format" => "YYYY-MM-DD HH:mm")
        end
      end
    end
  end
end
