Rails.application.routes.draw do
  resources :roles
  resources :forms do
    resources :submissions
  end
  resources :domains
  resources :abilities
  resources :abilitysets
  resources :masteries
  resources :contacts, path: '/account/contacts'
  resources :links
  resources :locations
  resources :events do
    collection do
      get 'list'
      get 'tasks'
      get 'calendar'
      get 'log'
      get ':id/participants', to: 'events#participants', as: :participants
      get ':id/login',  to: 'events#login',  as: :login
      get ':id/logout', to: 'events#logout', as: :logout
      patch ':id/login',  to: 'events#login_update', as: :login_update
      patch ':id/logout',  to: 'events#logout_update', as: :logout_update
      patch 'login',  to: 'events#login_updates', as: :login_updates
      patch 'logout',  to: 'events#logout_updates', as: :logout_updates
    end
  end

  resources :conversations do
    member do
      post :reply
      post :trash
      post :untrash
    end
    collection do
      delete :empty_trash
    end
  end

  resources :notes
  get 'dashboard/index'
  get 'conversations/load_messages'
  get "mail" => "mailbox#inbox", as: :mailbox
  get "mail/inbox" => "mailbox#inbox", as: :mailbox_inbox
  get "mail/sent" => "mailbox#sent", as: :mailbox_sent
  get "mail/trash" => "mailbox#trash", as: :mailbox_trash

  resources :users do
    member do
      put :activate
      put :deactivate
      get :mastery
      get :contacts
      put :update_mastery
    end
  end
  devise_for :users, :path => '/account'
  devise_scope :user do
    authenticated :user do
      root 'dashboard#index', as: :authenticated_root
      root :to => 'dashboard#index'

    end
    unauthenticated do
      root 'devise/sessions#new', as: :unauthenticated_root
    end
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
