class AddAccountTypeToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :account_type, :string, null: false, default: ""
  end
end
