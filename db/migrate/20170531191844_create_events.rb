class CreateEvents < ActiveRecord::Migration[5.1]
  def change
    create_table :events do |t|
      t.references :overseer
      t.references :student
      t.references :location
      t.datetime :start
      t.datetime :stop
      t.datetime :login
      t.datetime :logout

      t.timestamps
    end
  end
end
