class CreateLocations < ActiveRecord::Migration[5.1]
  def change
    create_table :locations do |t|
      t.jsonb :address_line1, null: false, default: {}
      t.jsonb :address_line2, null: false, default: {}
      t.string :country, null: false, default: ""
      t.jsonb :region, null: false, default: {}
      t.jsonb :city, null: false, default: {}
      t.jsonb :zip, null: false, default: {}
      t.jsonb :name, null: false, default: {}
      t.jsonb :unit, null: false, default: {}

      t.timestamps
    end
  end
end
