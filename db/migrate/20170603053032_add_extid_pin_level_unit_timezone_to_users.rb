class AddExtidPinLevelUnitTimezoneToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :extid,    :string,  null: false, default: ""
    add_column :users, :pin,      :string,  null: false, default: ""
    add_column :users, :level,    :integer, null: false, default: 0
    add_column :users, :unit,     :string,  null: false, default: ""
    add_column :users, :timezone, :string,  null: false, default: "UTC"
  end
end
