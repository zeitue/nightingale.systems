class CreateContacts < ActiveRecord::Migration[5.1]
  def change
    create_table :contacts do |t|
      t.string :service, null: false, default: ""
      t.string :identity, null: false, default: ""
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
