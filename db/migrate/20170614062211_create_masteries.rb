class CreateMasteries < ActiveRecord::Migration[5.1]
  def change
    create_table :masteries do |t|
      t.jsonb :title, null: false, default: {}

      t.timestamps
    end
  end
end
