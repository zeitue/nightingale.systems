class CreateAbilitysets < ActiveRecord::Migration[5.1]
  def change
    create_table :abilitysets do |t|
      t.jsonb :title, null: false, default: {}
      t.integer :level

      t.timestamps
    end
  end
end
