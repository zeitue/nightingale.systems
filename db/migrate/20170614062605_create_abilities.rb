class CreateAbilities < ActiveRecord::Migration[5.1]
  def change
    create_table :abilities do |t|
      t.jsonb :title, null: false, default: {}
      t.jsonb :category, null: false, default: {}
      t.integer :required_level
      t.integer :proficiency_level
      t.timestamps
    end
  end
end
