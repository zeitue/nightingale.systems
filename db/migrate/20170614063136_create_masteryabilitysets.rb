class CreateMasteryabilitysets < ActiveRecord::Migration[5.1]
  def change
    create_table :masteryabilitysets do |t|
      t.references :mastery, foreign_key: true
      t.references :abilityset, foreign_key: true

      t.timestamps
    end

    add_index :masteryabilitysets, [:mastery_id, :abilityset_id], unique: true
  end
end
