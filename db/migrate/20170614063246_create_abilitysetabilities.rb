class CreateAbilitysetabilities < ActiveRecord::Migration[5.1]
  def change
    create_table :abilitysetabilities do |t|
      t.references :abilityset, foreign_key: true
      t.references :ability, foreign_key: true

      t.timestamps
    end

    add_index :abilitysetabilities, [:abilityset_id, :ability_id], unique: true
  end
end
