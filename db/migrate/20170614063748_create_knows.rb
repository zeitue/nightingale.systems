class CreateKnows < ActiveRecord::Migration[5.1]
  def change
    create_table :knows do |t|
      t.references :user, foreign_key: true
      t.references :ability, foreign_key: true
      t.integer :proficiency, null: false, default: 0

      t.timestamps
    end

    add_index :knows, [:user_id, :ability_id], unique: true
  end
end
