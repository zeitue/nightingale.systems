class AddAbilitysetToEvents < ActiveRecord::Migration[5.1]
  def change
    add_reference :events, :abilityset, foreign_key: true
  end
end
