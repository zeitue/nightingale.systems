class AddMasteryToUser < ActiveRecord::Migration[5.1]
  def change
    add_reference :users, :mastery, foreign_key: true
  end
end
