class AddForeignKeyConstraintsToEvents < ActiveRecord::Migration[5.1]
  def change
    add_foreign_key :events, :locations, column: :location_id
    add_foreign_key :events, :users, column: :student_id
    add_foreign_key :events, :users, column: :overseer_id
  end
end
