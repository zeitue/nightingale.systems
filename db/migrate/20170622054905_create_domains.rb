class CreateDomains < ActiveRecord::Migration[5.1]
  def change
    create_table :domains do |t|
      t.jsonb :name, null: false, default: {}
      t.string :namespace, default: ''
      t.string :language, default: 'en'
      t.string :timezone, default: 'UTC'
      t.string :country, default: ''
      t.integer :student_starting_level, default: 1
      t.timestamps
    end

    add_index :domains, :namespace, :unique => true
  end
end
