class AddDomain < ActiveRecord::Migration[5.1]
  def change
    add_reference :events, :domain, foreign_key: true
    add_reference :users, :domain, foreign_key: true
    add_reference :links, :domain, foreign_key: true
    add_reference :locations, :domain, foreign_key: true
    add_reference :masteries, :domain, foreign_key: true
    add_reference :abilitysets, :domain, foreign_key: true
    add_reference :abilities, :domain, foreign_key: true
  end
end
