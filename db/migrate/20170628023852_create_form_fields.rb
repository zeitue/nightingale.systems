class CreateFormFields < ActiveRecord::Migration[5.1]
  def change
    create_table :form_fields do |t|
      t.references :form, foreign_key: true
      t.string :field_type
      t.jsonb :label
      t.jsonb :text
      t.boolean :required
      t.integer :position
      t.jsonb :tooltip
      t.jsonb :placeholder
      t.float :min
      t.float :max
      t.integer :rows

      t.timestamps
    end
  end
end
