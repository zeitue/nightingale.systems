class CreateFieldOptions < ActiveRecord::Migration[5.1]
  def change
    create_table :field_options do |t|
      t.references :form_field, foreign_key: true
      t.jsonb :option_key
      t.string :option_value

      t.timestamps
    end
  end
end
