class CreateTextResponses < ActiveRecord::Migration[5.1]
  def change
    create_table :text_responses do |t|
      t.references :submission, foreign_key: true
      t.references :form_field, foreign_key: true
      t.text :answer

      t.timestamps
    end
  end
end
