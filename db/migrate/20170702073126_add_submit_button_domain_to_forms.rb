class AddSubmitButtonDomainToForms < ActiveRecord::Migration[5.1]
  def change
    add_column :forms, :submit_button, :jsonb, default: ""
    add_reference :forms, :domain, foreign_key: true
  end
end
