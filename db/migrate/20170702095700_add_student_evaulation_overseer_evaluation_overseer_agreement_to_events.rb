class AddStudentEvaulationOverseerEvaluationOverseerAgreementToEvents < ActiveRecord::Migration[5.1]
  def change
    add_reference :events, :student_evaluation
    add_reference :events, :overseer_evaluation
    add_reference :events, :overseer_agreement
  end
end
