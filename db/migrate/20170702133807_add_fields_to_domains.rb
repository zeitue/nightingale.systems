class AddFieldsToDomains < ActiveRecord::Migration[5.1]
  def change
    add_column :domains, :map_provider, :string
    add_column :domains, :student_form_fill, :string
    add_column :domains, :overseer_form_fill, :string
    add_column :domains, :overseer_agreement_form_fill, :string
    add_reference :domains, :student_form
    add_reference :domains, :overseer_form
    add_reference :domains, :overseer_agreement_form
    add_column :domains, :time_margin, :integer
  end
end
