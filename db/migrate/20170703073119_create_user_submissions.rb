class CreateUserSubmissions < ActiveRecord::Migration[5.1]
  def change
    create_table :user_submissions do |t|
      t.references :user, foreign_key: true
      t.references :submission, foreign_key: true

      t.timestamps
    end
    add_index :user_submissions, [:user_id, :submission_id], unique: true
  end
end
