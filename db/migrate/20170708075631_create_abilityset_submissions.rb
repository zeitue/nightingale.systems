class CreateAbilitysetSubmissions < ActiveRecord::Migration[5.1]
  def change
    create_table :abilityset_submissions do |t|
      t.references :abilityset, foreign_key: true
      t.references :submission, foreign_key: true

      t.timestamps
    end
    add_index :abilityset_submissions, [:abilityset_id, :submission_id], unique: true
  end
end
