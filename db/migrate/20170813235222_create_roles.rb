class CreateRoles < ActiveRecord::Migration[5.1]
  def change
    create_table :roles do |t|
      t.jsonb :title, null: false, default: {}
      t.boolean :use_events, null: false, default: true
      t.boolean :events_has_calendar, null: false, default: true
      t.boolean :events_has_list, null: false, default: true
      t.boolean :events_has_tasks, null: false, default: false
      t.boolean :events_has_log, null: false, default: false
      t.boolean :events_create_edit, null: false, default: false
      t.boolean :events_sign_in, null: false, default: false
      t.boolean :use_notes, null: false, default: true
      t.boolean :use_mail, null: false, default: true
      t.boolean :use_user_directory, null: false, default: true
      t.boolean :use_links, null: false, default: true
      t.boolean :links_create_edit, null: false, default: false
      t.boolean :use_locations, null: false, default: false
      t.boolean :use_mastery_builder, null: false, default: false
      t.boolean :has_mastery, null: false, default: false
      t.boolean :mastery_rank_up, null: false, default: false
      t.boolean :use_form_builder, null: false, default: false
      t.references :domain, foreign_key: true

      t.timestamps
    end
  end
end
