class AddUseFormsToRoles < ActiveRecord::Migration[5.1]
  def change
    add_column :roles, :use_forms, :boolean, null: false, default: true
  end
end
