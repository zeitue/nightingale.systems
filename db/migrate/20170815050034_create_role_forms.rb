class CreateRoleForms < ActiveRecord::Migration[5.1]
  def change
    create_table :role_forms do |t|
      t.references :role, foreign_key: true
      t.references :form, foreign_key: true
      t.string :when, null: false, default: ""

      t.timestamps
    end
  end
end
