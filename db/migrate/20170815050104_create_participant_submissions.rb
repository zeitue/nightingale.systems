class CreateParticipantSubmissions < ActiveRecord::Migration[5.1]
  def change
    create_table :participant_submissions do |t|
      t.references :participant, foreign_key: true
      t.references :submission, foreign_key: true

      t.timestamps
    end
  end
end
