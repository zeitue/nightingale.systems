class RemoveOverseerStudentStudentEvaluationOverseerEvaluationOverseerAgreementFromEvents < ActiveRecord::Migration[5.1]
  def change
    remove_reference :events, :overseer
    remove_reference :events, :student
    remove_reference :events, :student_evaluation
    remove_reference :events, :overseer_evaluation
    remove_reference :events, :overseer_agreement
  end
end
