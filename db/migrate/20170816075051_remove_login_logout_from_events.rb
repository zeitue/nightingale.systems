class RemoveLoginLogoutFromEvents < ActiveRecord::Migration[5.1]
  def change
    remove_column :events, :login, :datetime
    remove_column :events, :logout, :datetime
  end
end
