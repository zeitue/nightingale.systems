class FixEventsListOnRoles < ActiveRecord::Migration[5.1]
  def up
    rename_column :roles, :events_has_list, :events_overseer
  end

  def down
    rename_column :roles, :events_overseer, :events_has_list
  end
end
