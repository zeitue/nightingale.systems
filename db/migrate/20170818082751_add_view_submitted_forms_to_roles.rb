class AddViewSubmittedFormsToRoles < ActiveRecord::Migration[5.1]
  def change
    add_column :roles, :view_submitted_forms, :boolean, null: false, default: false
  end
end
