class AddCompletedToParticipants < ActiveRecord::Migration[5.1]
  def change
    add_column :participants, :completed, :boolean, null: false, default: false
  end
end
