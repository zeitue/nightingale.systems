class RemoveStudentFormFillOverseerFormFillOverseerAgreementFormFillStudentFormOverseerFormOverseerAgreementFormFromDomains < ActiveRecord::Migration[5.1]
  def change
    remove_column :domains, :overseer_agreement_form_fill
    remove_column :domains, :student_form_fill
    remove_column :domains, :overseer_form_fill
    remove_column :domains, :overseer_form_id
    remove_column :domains, :student_form_id
    remove_column :domains, :overseer_agreement_form_id
  end
end
