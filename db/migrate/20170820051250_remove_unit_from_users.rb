class RemoveUnitFromUsers < ActiveRecord::Migration[5.1]
  def change
    remove_column :users, :unit, :string
  end
end
