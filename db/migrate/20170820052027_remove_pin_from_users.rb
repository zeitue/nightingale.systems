class RemovePinFromUsers < ActiveRecord::Migration[5.1]
  def change
    remove_column :users, :pin, :string
  end
end
