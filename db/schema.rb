# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170820052027) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "abilities", force: :cascade do |t|
    t.jsonb "title", default: {}, null: false
    t.jsonb "category", default: {}, null: false
    t.integer "required_level"
    t.integer "proficiency_level"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "domain_id"
    t.index ["domain_id"], name: "index_abilities_on_domain_id"
  end

  create_table "abilityset_submissions", force: :cascade do |t|
    t.bigint "abilityset_id"
    t.bigint "submission_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["abilityset_id", "submission_id"], name: "index_abilityset_submissions_on_abilityset_id_and_submission_id", unique: true
    t.index ["abilityset_id"], name: "index_abilityset_submissions_on_abilityset_id"
    t.index ["submission_id"], name: "index_abilityset_submissions_on_submission_id"
  end

  create_table "abilitysetabilities", force: :cascade do |t|
    t.bigint "abilityset_id"
    t.bigint "ability_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["ability_id"], name: "index_abilitysetabilities_on_ability_id"
    t.index ["abilityset_id", "ability_id"], name: "index_abilitysetabilities_on_abilityset_id_and_ability_id", unique: true
    t.index ["abilityset_id"], name: "index_abilitysetabilities_on_abilityset_id"
  end

  create_table "abilitysets", force: :cascade do |t|
    t.jsonb "title", default: {}, null: false
    t.integer "level"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "domain_id"
    t.index ["domain_id"], name: "index_abilitysets_on_domain_id"
  end

  create_table "contacts", force: :cascade do |t|
    t.string "service", default: "", null: false
    t.string "identity", default: "", null: false
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_contacts_on_user_id"
  end

  create_table "domains", force: :cascade do |t|
    t.jsonb "name", default: {}, null: false
    t.string "namespace", default: ""
    t.string "language", default: "en"
    t.string "timezone", default: "UTC"
    t.string "country", default: ""
    t.integer "student_starting_level", default: 1
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "map_provider"
    t.integer "time_margin"
    t.boolean "demo", default: false, null: false
    t.index ["namespace"], name: "index_domains_on_namespace", unique: true
  end

  create_table "events", force: :cascade do |t|
    t.bigint "location_id"
    t.datetime "start"
    t.datetime "stop"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "user_id"
    t.bigint "abilityset_id"
    t.bigint "domain_id"
    t.integer "participants_count", default: 0
    t.index ["abilityset_id"], name: "index_events_on_abilityset_id"
    t.index ["domain_id"], name: "index_events_on_domain_id"
    t.index ["location_id"], name: "index_events_on_location_id"
    t.index ["user_id"], name: "index_events_on_user_id"
  end

  create_table "field_options", force: :cascade do |t|
    t.bigint "form_field_id"
    t.jsonb "option_key"
    t.string "option_value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["form_field_id"], name: "index_field_options_on_form_field_id"
  end

  create_table "form_fields", force: :cascade do |t|
    t.bigint "form_id"
    t.string "field_type"
    t.jsonb "label"
    t.jsonb "text"
    t.boolean "required"
    t.integer "position"
    t.jsonb "tooltip"
    t.jsonb "placeholder"
    t.float "min"
    t.float "max"
    t.integer "rows"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["form_id"], name: "index_form_fields_on_form_id"
  end

  create_table "forms", force: :cascade do |t|
    t.jsonb "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.jsonb "submit_button", default: ""
    t.bigint "domain_id"
    t.index ["domain_id"], name: "index_forms_on_domain_id"
  end

  create_table "knows", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "ability_id"
    t.integer "proficiency", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["ability_id"], name: "index_knows_on_ability_id"
    t.index ["user_id", "ability_id"], name: "index_knows_on_user_id_and_ability_id", unique: true
    t.index ["user_id"], name: "index_knows_on_user_id"
  end

  create_table "links", force: :cascade do |t|
    t.jsonb "title", default: {}, null: false
    t.string "url"
    t.integer "account_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "domain_id"
    t.index ["domain_id"], name: "index_links_on_domain_id"
  end

  create_table "locations", force: :cascade do |t|
    t.jsonb "address_line1", default: {}, null: false
    t.jsonb "address_line2", default: {}, null: false
    t.string "country", default: "", null: false
    t.jsonb "region", default: {}, null: false
    t.jsonb "city", default: {}, null: false
    t.jsonb "zip", default: {}, null: false
    t.jsonb "name", default: {}, null: false
    t.jsonb "unit", default: {}, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "domain_id"
    t.index ["domain_id"], name: "index_locations_on_domain_id"
  end

  create_table "mailboxer_conversation_opt_outs", id: :serial, force: :cascade do |t|
    t.string "unsubscriber_type"
    t.integer "unsubscriber_id"
    t.integer "conversation_id"
    t.index ["conversation_id"], name: "index_mailboxer_conversation_opt_outs_on_conversation_id"
    t.index ["unsubscriber_id", "unsubscriber_type"], name: "index_mailboxer_conversation_opt_outs_on_unsubscriber_id_type"
  end

  create_table "mailboxer_conversations", id: :serial, force: :cascade do |t|
    t.string "subject", default: ""
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "mailboxer_notifications", id: :serial, force: :cascade do |t|
    t.string "type"
    t.text "body"
    t.string "subject", default: ""
    t.string "sender_type"
    t.integer "sender_id"
    t.integer "conversation_id"
    t.boolean "draft", default: false
    t.string "notification_code"
    t.string "notified_object_type"
    t.integer "notified_object_id"
    t.string "attachment"
    t.datetime "updated_at", null: false
    t.datetime "created_at", null: false
    t.boolean "global", default: false
    t.datetime "expires"
    t.index ["conversation_id"], name: "index_mailboxer_notifications_on_conversation_id"
    t.index ["notified_object_id", "notified_object_type"], name: "index_mailboxer_notifications_on_notified_object_id_and_type"
    t.index ["notified_object_type", "notified_object_id"], name: "mailboxer_notifications_notified_object"
    t.index ["sender_id", "sender_type"], name: "index_mailboxer_notifications_on_sender_id_and_sender_type"
    t.index ["type"], name: "index_mailboxer_notifications_on_type"
  end

  create_table "mailboxer_receipts", id: :serial, force: :cascade do |t|
    t.string "receiver_type"
    t.integer "receiver_id"
    t.integer "notification_id", null: false
    t.boolean "is_read", default: false
    t.boolean "trashed", default: false
    t.boolean "deleted", default: false
    t.string "mailbox_type", limit: 25
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "is_delivered", default: false
    t.string "delivery_method"
    t.string "message_id"
    t.index ["notification_id"], name: "index_mailboxer_receipts_on_notification_id"
    t.index ["receiver_id", "receiver_type"], name: "index_mailboxer_receipts_on_receiver_id_and_receiver_type"
  end

  create_table "masteries", force: :cascade do |t|
    t.jsonb "title", default: {}, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "domain_id"
    t.index ["domain_id"], name: "index_masteries_on_domain_id"
  end

  create_table "masteryabilitysets", force: :cascade do |t|
    t.bigint "mastery_id"
    t.bigint "abilityset_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["abilityset_id"], name: "index_masteryabilitysets_on_abilityset_id"
    t.index ["mastery_id", "abilityset_id"], name: "index_masteryabilitysets_on_mastery_id_and_abilityset_id", unique: true
    t.index ["mastery_id"], name: "index_masteryabilitysets_on_mastery_id"
  end

  create_table "notes", force: :cascade do |t|
    t.string "title"
    t.text "body"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_notes_on_user_id"
  end

  create_table "participant_submissions", force: :cascade do |t|
    t.bigint "participant_id"
    t.bigint "submission_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["participant_id"], name: "index_participant_submissions_on_participant_id"
    t.index ["submission_id"], name: "index_participant_submissions_on_submission_id"
  end

  create_table "participants", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "event_id"
    t.datetime "login"
    t.datetime "logout"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "role_id"
    t.boolean "completed", default: false, null: false
    t.index ["event_id"], name: "index_participants_on_event_id"
    t.index ["role_id"], name: "index_participants_on_role_id"
    t.index ["user_id"], name: "index_participants_on_user_id"
  end

  create_table "role_forms", force: :cascade do |t|
    t.bigint "role_id"
    t.bigint "form_id"
    t.string "when", default: "", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["form_id"], name: "index_role_forms_on_form_id"
    t.index ["role_id"], name: "index_role_forms_on_role_id"
  end

  create_table "roles", force: :cascade do |t|
    t.jsonb "title", default: {}, null: false
    t.boolean "use_events", default: true, null: false
    t.boolean "events_has_calendar", default: true, null: false
    t.boolean "events_overseer", default: true, null: false
    t.boolean "events_has_tasks", default: false, null: false
    t.boolean "events_has_log", default: false, null: false
    t.boolean "events_create_edit", default: false, null: false
    t.boolean "events_sign_in", default: false, null: false
    t.boolean "use_notes", default: true, null: false
    t.boolean "use_mail", default: true, null: false
    t.boolean "use_user_directory", default: true, null: false
    t.boolean "use_links", default: true, null: false
    t.boolean "links_create_edit", default: false, null: false
    t.boolean "use_locations", default: false, null: false
    t.boolean "use_mastery_builder", default: false, null: false
    t.boolean "has_mastery", default: false, null: false
    t.boolean "mastery_rank_up", default: false, null: false
    t.boolean "use_form_builder", default: false, null: false
    t.bigint "domain_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "use_forms", default: true, null: false
    t.boolean "view_submitted_forms", default: false, null: false
    t.index ["domain_id"], name: "index_roles_on_domain_id"
  end

  create_table "submissions", force: :cascade do |t|
    t.bigint "form_id"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "submitted_at"
    t.index ["form_id"], name: "index_submissions_on_form_id"
    t.index ["user_id"], name: "index_submissions_on_user_id"
  end

  create_table "text_responses", force: :cascade do |t|
    t.bigint "submission_id"
    t.bigint "form_field_id"
    t.text "answer"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["form_field_id"], name: "index_text_responses_on_form_field_id"
    t.index ["submission_id"], name: "index_text_responses_on_submission_id"
  end

  create_table "user_submissions", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "submission_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["submission_id"], name: "index_user_submissions_on_submission_id"
    t.index ["user_id", "submission_id"], name: "index_user_submissions_on_user_id_and_submission_id", unique: true
    t.index ["user_id"], name: "index_user_submissions_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "prefix", default: "", null: false
    t.string "first_name", default: "", null: false
    t.string "middle_name", default: "", null: false
    t.string "last_name", default: "", null: false
    t.string "suffix", default: "", null: false
    t.string "language", default: "en", null: false
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.integer "failed_attempts", default: 0, null: false
    t.string "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "account_type", default: "", null: false
    t.string "extid", default: "", null: false
    t.integer "level", default: 0, null: false
    t.string "timezone", default: "UTC", null: false
    t.boolean "active", default: false, null: false
    t.date "birthdate"
    t.json "avatars"
    t.bigint "mastery_id"
    t.bigint "domain_id"
    t.bigint "role_id"
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["domain_id"], name: "index_users_on_domain_id"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["mastery_id"], name: "index_users_on_mastery_id"
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["role_id"], name: "index_users_on_role_id"
    t.index ["unlock_token"], name: "index_users_on_unlock_token", unique: true
  end

  add_foreign_key "abilities", "domains"
  add_foreign_key "abilityset_submissions", "abilitysets"
  add_foreign_key "abilityset_submissions", "submissions"
  add_foreign_key "abilitysetabilities", "abilities"
  add_foreign_key "abilitysetabilities", "abilitysets"
  add_foreign_key "abilitysets", "domains"
  add_foreign_key "contacts", "users"
  add_foreign_key "events", "abilitysets"
  add_foreign_key "events", "domains"
  add_foreign_key "events", "locations"
  add_foreign_key "events", "users"
  add_foreign_key "field_options", "form_fields"
  add_foreign_key "form_fields", "forms"
  add_foreign_key "forms", "domains"
  add_foreign_key "knows", "abilities"
  add_foreign_key "knows", "users"
  add_foreign_key "links", "domains"
  add_foreign_key "locations", "domains"
  add_foreign_key "mailboxer_conversation_opt_outs", "mailboxer_conversations", column: "conversation_id", name: "mb_opt_outs_on_conversations_id"
  add_foreign_key "mailboxer_notifications", "mailboxer_conversations", column: "conversation_id", name: "notifications_on_conversation_id"
  add_foreign_key "mailboxer_receipts", "mailboxer_notifications", column: "notification_id", name: "receipts_on_notification_id"
  add_foreign_key "masteries", "domains"
  add_foreign_key "masteryabilitysets", "abilitysets"
  add_foreign_key "masteryabilitysets", "masteries"
  add_foreign_key "notes", "users"
  add_foreign_key "participant_submissions", "participants"
  add_foreign_key "participant_submissions", "submissions"
  add_foreign_key "participants", "events"
  add_foreign_key "participants", "roles"
  add_foreign_key "participants", "users"
  add_foreign_key "role_forms", "forms"
  add_foreign_key "role_forms", "roles"
  add_foreign_key "roles", "domains"
  add_foreign_key "submissions", "forms"
  add_foreign_key "submissions", "users"
  add_foreign_key "text_responses", "form_fields"
  add_foreign_key "text_responses", "submissions"
  add_foreign_key "user_submissions", "submissions"
  add_foreign_key "user_submissions", "users"
  add_foreign_key "users", "domains"
  add_foreign_key "users", "masteries"
  add_foreign_key "users", "roles"
end
