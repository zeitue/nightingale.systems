# coding: utf-8
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Domain.create(name: "Nightingale", namespace: "nightingale",
              language: "en", timezone: "Central Time (US & Canada)",
              country: "US", student_starting_level: 1,
              map_provider: "google_maps", time_margin: 15)
