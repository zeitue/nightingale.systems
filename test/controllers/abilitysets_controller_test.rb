require 'test_helper'

class AbilitysetsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @abilityset = abilitysets(:one)
  end

  test "should get index" do
    get abilitysets_url
    assert_response :success
  end

  test "should get new" do
    get new_abilityset_url
    assert_response :success
  end

  test "should create abilityset" do
    assert_difference('Abilityset.count') do
      post abilitysets_url, params: { abilityset: { level: @abilityset.level, title: @abilityset.title } }
    end

    assert_redirected_to abilityset_url(Abilityset.last)
  end

  test "should show abilityset" do
    get abilityset_url(@abilityset)
    assert_response :success
  end

  test "should get edit" do
    get edit_abilityset_url(@abilityset)
    assert_response :success
  end

  test "should update abilityset" do
    patch abilityset_url(@abilityset), params: { abilityset: { level: @abilityset.level, title: @abilityset.title } }
    assert_redirected_to abilityset_url(@abilityset)
  end

  test "should destroy abilityset" do
    assert_difference('Abilityset.count', -1) do
      delete abilityset_url(@abilityset)
    end

    assert_redirected_to abilitysets_url
  end
end
