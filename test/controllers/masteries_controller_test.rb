require 'test_helper'

class MasteriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @mastery = masteries(:one)
  end

  test "should get index" do
    get masteries_url
    assert_response :success
  end

  test "should get new" do
    get new_mastery_url
    assert_response :success
  end

  test "should create mastery" do
    assert_difference('Mastery.count') do
      post masteries_url, params: { mastery: { title: @mastery.title } }
    end

    assert_redirected_to mastery_url(Mastery.last)
  end

  test "should show mastery" do
    get mastery_url(@mastery)
    assert_response :success
  end

  test "should get edit" do
    get edit_mastery_url(@mastery)
    assert_response :success
  end

  test "should update mastery" do
    patch mastery_url(@mastery), params: { mastery: { title: @mastery.title } }
    assert_redirected_to mastery_url(@mastery)
  end

  test "should destroy mastery" do
    assert_difference('Mastery.count', -1) do
      delete mastery_url(@mastery)
    end

    assert_redirected_to masteries_url
  end
end
