require 'test_helper'

class RolesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @role = roles(:one)
  end

  test "should get index" do
    get roles_url
    assert_response :success
  end

  test "should get new" do
    get new_role_url
    assert_response :success
  end

  test "should create role" do
    assert_difference('Role.count') do
      post roles_url, params: { role: { domain_id: @role.domain_id, events_create_edit: @role.events_create_edit, events_has_calendar: @role.events_has_calendar, events_has_list: @role.events_has_list, events_has_log: @role.events_has_log, events_has_tasks: @role.events_has_tasks, events_sign_in: @role.events_sign_in, has_mastery: @role.has_mastery, links_create_edit: @role.links_create_edit, mastery_rank_up: @role.mastery_rank_up, title: @role.title, use_events: @role.use_events, use_form_builder: @role.use_form_builder, use_links: @role.use_links, use_locations: @role.use_locations, use_mail: @role.use_mail, use_mastery_builder: @role.use_mastery_builder, use_notes: @role.use_notes, use_user_directory: @role.use_user_directory } }
    end

    assert_redirected_to role_url(Role.last)
  end

  test "should show role" do
    get role_url(@role)
    assert_response :success
  end

  test "should get edit" do
    get edit_role_url(@role)
    assert_response :success
  end

  test "should update role" do
    patch role_url(@role), params: { role: { domain_id: @role.domain_id, events_create_edit: @role.events_create_edit, events_has_calendar: @role.events_has_calendar, events_has_list: @role.events_has_list, events_has_log: @role.events_has_log, events_has_tasks: @role.events_has_tasks, events_sign_in: @role.events_sign_in, has_mastery: @role.has_mastery, links_create_edit: @role.links_create_edit, mastery_rank_up: @role.mastery_rank_up, title: @role.title, use_events: @role.use_events, use_form_builder: @role.use_form_builder, use_links: @role.use_links, use_locations: @role.use_locations, use_mail: @role.use_mail, use_mastery_builder: @role.use_mastery_builder, use_notes: @role.use_notes, use_user_directory: @role.use_user_directory } }
    assert_redirected_to role_url(@role)
  end

  test "should destroy role" do
    assert_difference('Role.count', -1) do
      delete role_url(@role)
    end

    assert_redirected_to roles_url
  end
end
